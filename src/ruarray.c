#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ruarray.h"

#include "memunittest.h"

void ruarray_swap(void * array, size_t elementSize, void * swapSpace, 
                  size_t index1, size_t index2)
{
   unsigned char * barray = array;
   bool ownMemory;
   if(swapSpace == NULL)
   {
      ownMemory = true;
      swapSpace = malloc(elementSize);
   }
   else
   {
      ownMemory = false;
   }
   memcpy(swapSpace, &barray[elementSize * index2], elementSize);
   memcpy(&barray[elementSize * index2], &barray[elementSize * index1], elementSize);
   memcpy(&barray[elementSize * index1], swapSpace, elementSize);
   
   if(ownMemory)
   {
      free(swapSpace);
   }
}

size_t ruarray_copy(void * array, size_t elementSize, size_t arraySize,
                    size_t toIndex, size_t fromIndex, size_t count)
{
   unsigned char * barray = array;
   size_t i;

   if(fromIndex >= arraySize ||
      toIndex   >= arraySize)
   {
      return 0;
   }

   // Saturate count
   if(fromIndex + count > arraySize)
   {
      count = arraySize - fromIndex;
   }
   if(toIndex + count > arraySize)
   {
      count = arraySize - toIndex;
   }
   
   if(count == 0)
   {
      return 0;
   }

   // Do the copy
   if(toIndex == fromIndex)
   {
      return count;
   }
   else if(toIndex < fromIndex)
   {
      for(i = 0; i < count; i++)
      {
         memcpy(&barray[elementSize * (toIndex + i)], &barray[elementSize * (fromIndex + i)], elementSize);
      }
   }
   else
   {
      for(i = count - 1; i < count; i--)
      {
         memcpy(&barray[elementSize * (toIndex + i)], &barray[elementSize * (fromIndex + i)], elementSize);
      }
   }
   
   return count;
}


