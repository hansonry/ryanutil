#include <stdlib.h>
#include <string.h>
#include "rumem.h"
#include "ruarray.h"

#include "memunittest.h"

#define rumem_getbase(mem) ((unsigned char**)(((unsigned char *)(mem)) + (mem)->baseByteOffset)) 

#define DEFAULT_INITSIZE 32

void rumem_init(struct rumem * mem, void ** basePtr, size_t elementSize, size_t initSize)
{
   unsigned char ** base;
   mem->baseByteOffset = (int)((unsigned char *)basePtr - (unsigned char *)mem);
   base = rumem_getbase(mem);

   if(initSize == 0)
   {
      initSize = DEFAULT_INITSIZE;
   }

   mem->elementSize = elementSize;
   mem->swapSpace = NULL;

   mem->size = initSize;
   *base = malloc(mem->elementSize * mem->size);

}

void rumem_destroy(struct rumem * mem)
{
   unsigned char ** base;
   base = rumem_getbase(mem);
   if(mem->swapSpace != NULL)
   {
      free(mem->swapSpace);
      mem->swapSpace = NULL;
   }
   free(*base);
   *base = NULL;
   mem->size        = 0;
   mem->elementSize = 0;
}

void rumem_growto(struct rumem * mem, size_t targetSize)
{
   unsigned char ** base;
   if(mem->size < targetSize)
   {
      base = rumem_getbase(mem);
      mem->size = targetSize;
      *base = realloc(*base, mem->elementSize * mem->size);
   }
}

void rumem_shrinkto(struct rumem * mem, size_t targetSize)
{
   unsigned char ** base;
   if(mem->size > targetSize)
   {
      base = rumem_getbase(mem);
      mem->size = targetSize;
      *base = realloc(*base, mem->elementSize * mem->size);
   }
}

void * rumem_get(struct rumem * mem, size_t index)
{
   unsigned char ** base;
   if(index < mem->size)
   {
      base = rumem_getbase(mem);
      return &(*base)[mem->elementSize * index];
   }
   return NULL;
}


enum rustatus rumem_copyin(struct rumem * mem, size_t index, const void * data)
{
   unsigned char ** base;
   if(index >= mem->size || data == NULL)
   {
      return e_rus_failure;
   }
   base = rumem_getbase(mem);
   memcpy(&(*base)[mem->elementSize * index], data, mem->elementSize);
   return e_rus_success;
}

size_t rumem_copyinto(struct rumem * mem, const void * data, size_t index, size_t size)
{
   size_t i;
   unsigned char ** base;
   size_t offset;
   
   if(index >= mem->size || size == 0 || data == NULL)
   {
      return 0;
   }
   if(index + size > mem->size)
   {
      size = mem->size - index;
   }
   offset = mem->elementSize * index;
   base = rumem_getbase(mem);
   for(i = index; i < (index + size); i++)
   {
      memcpy(&(*base)[offset], data, mem->elementSize);
      offset += mem->elementSize;
   }

   return size;
}

void rumem_copyinall(struct rumem * mem, const void * data)
{
   size_t i;
   unsigned char ** base;
   size_t offset = 0;
   if(data != NULL)
   {
      base = rumem_getbase(mem);
      for(i = 0; i < mem->size; i++)
      {
         memcpy(&(*base)[offset], data, mem->elementSize);
         offset += mem->elementSize;
      }
   }
}

enum rustatus rumem_copyout(struct rumem * mem, void * data, size_t index)
{
   unsigned char ** base;
   if(index >= mem->size || data == NULL)
   {
      return e_rus_failure;
   }

   base = rumem_getbase(mem);
   memcpy(data, &(*base)[mem->elementSize * index], mem->elementSize);
   return e_rus_success;
}

enum rustatus rumem_swap(struct rumem * mem, size_t index1, size_t index2)
{
   unsigned char ** base;
   if(index1 >= mem->size || index2 >= mem->size)
   {
      return e_rus_failure;
   }

   if(index1 == index2)
   {
      return e_rus_success;
   }

   base = rumem_getbase(mem);
   if(mem->swapSpace == NULL)
   {
      mem->swapSpace = malloc(mem->elementSize);
   }
   ruarray_swap(*base, mem->elementSize, mem->swapSpace, index1, index2);

   return e_rus_success;
}

size_t rumem_blockcopy(struct rumem * mem, size_t toIndex, 
                       size_t fromIndex, size_t count)
{
   unsigned char ** base;

   base = rumem_getbase(mem);
   return ruarray_copy(*base, mem->elementSize, mem->size, 
                       toIndex, fromIndex, count);
}

size_t rumem_indexof(struct rumem * mem, const void * address)
{

   unsigned char ** base;
   const unsigned char * baddress = address;
   base = rumem_getbase(mem);
   if(baddress <  *base ||
      baddress >= ((*base) + (mem->size * mem->elementSize)))
   {
      return mem->size;
   }
   
   return (baddress - *base) / mem->elementSize;
}

size_t rumem_size(struct rumem * mem)
{
   return mem->size;
}

