#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <rudense.h>

#include "memunittest.h"

#define DEFAULT_INITSIZE 0x500
#define DEFAULT_GROWBY   0x100

struct rud_entry
{
   size_t size;
   size_t next;
};

static void rudense_set_bitfield_at(struct rudense * dense, size_t address,
                                    size_t * components, size_t componentsSize)
{
   size_t i, k;
   unsigned char * bitfield;
   unsigned char mask;
   bitfield = &dense->base[address];
   if(components == NULL || componentsSize == 0)
   {
      for(i = 0; i < dense->bitFieldSize; i++)
      {
         bitfield[i] = 0;
      }
   }
   else
   {
      *bitfield = 0;
      mask = 1;
      for(i = 0; i < dense->componentTableSize; i++)
      {
         if((i % 8) == 0 && i != 0)
         {
            mask = 1;
            bitfield++;
            *bitfield = 0;
         }
         for(k = 0; k < componentsSize; k++)
         {
            if(components[k] == i)
            {
               *bitfield |= mask;
               break;
            }
         }
         mask = mask << 1;
      }
   }
}


static void rudense_create_jump_record(struct rudense * dense, 
                                       size_t address,
                                       size_t targetAddress)
{
   size_t * jumpAddress;
   rudense_set_bitfield_at(dense, address, NULL, 0);
   jumpAddress = (size_t*)&dense->base[address + dense->bitFieldSize];
   *jumpAddress = targetAddress;
}


void rudense_init(struct rudense * dense, size_t * componentTable, 
                                          size_t componentTableSize, 
                                          size_t initSize,
                                          size_t growBy)
{
   size_t numberOfBytesToFitBits;
   size_t remainingBytes;
   // Compute Bit Field Size
   numberOfBytesToFitBits = (componentTableSize + 7) / 8;
   remainingBytes = numberOfBytesToFitBits % sizeof(int);
   if(remainingBytes == 0)
   {
      dense->bitFieldSize = numberOfBytesToFitBits; 
   }
   else
   {
      dense->bitFieldSize = numberOfBytesToFitBits + 
                            (sizeof(int) - remainingBytes);
   }

   if(initSize == 0)
   {
      dense->size = DEFAULT_INITSIZE;
   }
   else if(initSize < (dense->bitFieldSize + sizeof(size_t))) 
   {
      dense->size = dense->bitFieldSize + sizeof(size_t);
   }
   else
   {
      dense->size = initSize;
   }

   if(growBy == 0)
   {
      dense->growBy = DEFAULT_GROWBY;
   }
   else
   {
      dense->growBy = growBy;
   }
   dense->base = malloc(dense->size);
   dense->count = 0;

   dense->componentTable = componentTable;
   dense->componentTableSize = componentTableSize;

   
   
   rudense_create_jump_record(dense, 0, 0);
}

void rudense_destroy(struct rudense * dense)
{
   free(dense->base);
   dense->base               = NULL;
   dense->size               = 0;
   dense->growBy             = 0;
   dense->count              = 0;
   dense->componentTable     = NULL;
   dense->componentTableSize = 0;
   dense->bitFieldSize       = 0;
}

static size_t rudense_get_addr_after_element(struct rudense * dense, 
                                             size_t address)
{
   unsigned char * bitfield;
   unsigned char mask;
   size_t nextAddress;
   size_t minNextAddress;
   size_t i;
   bool hasBit; 
   nextAddress = address + dense->bitFieldSize;
   bitfield = &dense->base[address];
   mask = 1;
   hasBit = false;
   for(i = 0; i < dense->componentTableSize; i++)
   {
      if((i % 8) == 0 && i != 0)
      {
         mask = 1;
         bitfield++;
      }
      if((*bitfield & mask) == mask)
      {
         nextAddress += dense->componentTable[i];
         hasBit = true;
      }
      mask = mask << 1;
   }
   
   if(hasBit)
   {
      minNextAddress = address + dense->bitFieldSize + sizeof(size_t);
      if(nextAddress < minNextAddress)
      {
         nextAddress = minNextAddress;
      }
      return nextAddress;
   }
   return address;
}

static bool rudense_is_empty(struct rudense * dense, size_t address)
{
   unsigned char * bitfield;
   unsigned char mask;
   size_t i;

   bitfield = &dense->base[address];
   mask = 1;
   for(i = 0; i < dense->componentTableSize; i++)
   {
      if((i % 8) == 0 && i != 0)
      {
         mask = 1;
         bitfield++;
      }
      if((*bitfield & mask) == mask)
      {
         return false;
      }
      mask = mask << 1;
   }
   return true;
}

static size_t rudense_get_addr_after_empty(struct rudense * dense, 
                                           size_t address)
{
   return *((size_t*)&dense->base[address + dense->bitFieldSize]);
}


bool rudense_has_room_for(struct rudense * dense, 
                          size_t startAddress, size_t nextAddress,
                          size_t * components, size_t componentsSize)
{
   size_t size = nextAddress - startAddress;
   size_t computedNextAddress;
   size_t * jumpAddress;
   if(size <= dense->bitFieldSize)
   {
      return false;
   }
   rudense_set_bitfield_at(dense, startAddress, components, componentsSize);
   computedNextAddress = rudense_get_addr_after_element(dense, startAddress);
   if(computedNextAddress > nextAddress)
   {
      rudense_set_bitfield_at(dense, startAddress, NULL, 0);
      return false;
   }
   if(computedNextAddress == nextAddress)
   {
      return true;
   }
   if((dense->bitFieldSize + sizeof(size_t)) > (nextAddress - computedNextAddress))
   {
      rudense_set_bitfield_at(dense, startAddress, NULL, 0);
      return false;
   }

   rudense_set_bitfield_at(dense, computedNextAddress, NULL, 0);
   jumpAddress = (size_t*)&dense->base[computedNextAddress + dense->bitFieldSize];
   *jumpAddress = nextAddress;
   return true;
}

static void rudense_makeroomfor(struct rudense * dense, size_t address,
                                                        size_t size)
{
   size_t newSize;
   newSize = dense->size;
   while(address + size > newSize)
   {
      newSize += dense->growBy;
   }

   if(newSize > dense->size)
   {
      dense->size = newSize;
      dense->base = realloc(dense->base, dense->size);
   }
}


size_t rudense_add(struct rudense * dense, size_t * components, 
                                           size_t componentsSize)
{
   size_t searchAddressThis, searchAddressNext, endCapAddress;
   size_t nextAddress;
   size_t prevAddress;
   bool addEndCap;

   searchAddressThis = 0;
   prevAddress = 1;
   addEndCap = true;
   while(searchAddressThis != prevAddress)
   {
      prevAddress = searchAddressThis;
      if(rudense_is_empty(dense, searchAddressThis))
      {
         searchAddressNext = rudense_get_addr_after_empty(dense, searchAddressThis);
         if(rudense_has_room_for(dense, searchAddressThis, searchAddressNext, 
                                 components, componentsSize))
         {
            addEndCap = false;
            break;
         }
         else
         {
            searchAddressThis = searchAddressNext;
         }
      }
      else
      {
         searchAddressThis = rudense_get_addr_after_element(dense, searchAddressThis);
      }
   }
   
   nextAddress = searchAddressThis;

   rudense_makeroomfor(dense, nextAddress, dense->bitFieldSize);
   rudense_set_bitfield_at(dense, nextAddress, components, componentsSize);
   
   if(addEndCap)
   {      
      endCapAddress = rudense_get_addr_after_element(dense, nextAddress);
      rudense_makeroomfor(dense, endCapAddress, dense->bitFieldSize + sizeof(size_t));
      rudense_create_jump_record(dense, endCapAddress, endCapAddress);
   }

   dense->count ++;

   return nextAddress;
}

void * rudense_get(struct rudense * dense, size_t address, size_t component)
{
   unsigned char * bitfield;
   unsigned char mask;
   size_t offset;
   size_t i;
   
   if(component >= dense->componentTableSize)
   {
      return NULL;
   }
   
   bitfield = &dense->base[address];
   mask = 1;
   offset = 0;
   for(i = 0; i <= component; i++)
   {
      if((i % 8) == 0 && i != 0)
      {
         mask = 1;
         bitfield++;
      }

      if((*bitfield & mask) == mask)
      {
         if(i < component)
         {
            offset += dense->componentTable[i];
         }
      }
      else
      {
         if(i == component)
         {
            return NULL;
         }
      }
      
      mask = mask << 1;
   }
   return &dense->base[address + dense->bitFieldSize + offset];
}

void * rudense_getall(struct rudense * dense, size_t address, void * data)
{
   unsigned char * bitfield;
   unsigned char mask;
   size_t i;
   size_t offset;
   void ** ptr_array;
   
   ptr_array = (void**)data;
   bitfield = &dense->base[address];
   mask = 1;
   offset = 0;
   for(i = 0; i < dense->componentTableSize; i++)
   {
      if((i % 8) == 0 && i != 0)
      {
         mask = 1;
         bitfield++;
      }

      if((*bitfield & mask) == mask)
      {
         ptr_array[i] = &dense->base[address + dense->bitFieldSize + offset];
         offset += dense->componentTable[i];
      }
      else
      {
         ptr_array[i] = NULL;
      }
      
      mask = mask << 1;
   }
   return data;
}

enum rustatus rudense_remove(struct rudense * dense, size_t address)
{
   size_t addressAfter;
   
   if(dense->count == 0 ||
      !rudense_isaddress(dense, address))
   {
      return e_rus_failure;
   }
   
   addressAfter = rudense_get_addr_after_element(dense, address);
   rudense_create_jump_record(dense, address, addressAfter);
   dense->count --;

   return e_rus_success;
}

bool rudense_isaddress(struct rudense * dense, size_t address)
{
   size_t check_address;
   size_t prevAddress;
   if(dense->count < 1)
   {
      return false;
   }
   
   check_address = 0;
   prevAddress = 1;
   while(prevAddress != check_address && check_address <= address)
   {
      prevAddress = check_address;
      if(rudense_is_empty(dense, check_address))
      {
         check_address = rudense_get_addr_after_empty(dense, check_address);
      }
      else
      {
         if(address == check_address)
         {
            return true;
         }
         check_address = rudense_get_addr_after_element(dense, check_address);
      }
      
   }
   return false;
}

void rudense_mergeunused(struct rudense * dense)
{
   size_t startAddress, endAddress;
   size_t prevAddress;
   
   startAddress = 0;
   prevAddress = 1;
   
   while(startAddress != prevAddress)
   {
      endAddress = startAddress;
      while(endAddress != prevAddress && rudense_is_empty(dense, endAddress))
      {
         prevAddress = endAddress;
         endAddress = rudense_get_addr_after_empty(dense, endAddress);
      }
      if(endAddress == prevAddress)
      {
         // We found the end. Caping it at the last good element
         rudense_create_jump_record(dense, startAddress, startAddress);
         startAddress = prevAddress;
      }
      else if(startAddress == endAddress)
      {
         // We found an element that was populated, move on
         prevAddress = startAddress;
         startAddress = rudense_get_addr_after_element(dense, startAddress);
      }
      else
      {
         // We found some space lets merge it all together into one
         // empty record
         rudense_create_jump_record(dense, startAddress, endAddress);
         prevAddress = endAddress;
         startAddress = rudense_get_addr_after_element(dense, endAddress);
      }
   }
   
}

size_t rudense_iterstart(struct rudense * dense)
{
   return dense->size;
}

bool rudense_iternext(struct rudense * dense, size_t * address)
{
   size_t prevAddress;
   if(address == NULL ||
      dense->count == 0)
   {
      return false;
   }
   
   
   if(*address == dense->size)
   {
      prevAddress = 1;
      *address = 0;
   }
   else
   {   
      prevAddress = *address;
      *address = rudense_get_addr_after_element(dense, *address);
   }
   
   while(rudense_is_empty(dense, *address) && prevAddress != *address)
   {
      prevAddress = *address;
      *address = rudense_get_addr_after_empty(dense, *address);
   }
   
   if(prevAddress == *address)
   {
      return false;
   }
   return true;
}

