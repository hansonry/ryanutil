#include <math.h>
#include <string.h>
#include "ruserialize.h"

void ruserialize_i8write(void * buffer, uint8_t value)
{
   uint8_t *lb = buffer;
   lb[0] = value;
}

uint8_t ruserialize_i8read(const void * buffer)
{
   const uint8_t *lb = buffer;
   return lb[0];
}

void     ruserialize_i16write(void * buffer, uint16_t value)
{
   uint8_t *lb = buffer;
   lb[0] = (uint8_t)(value >> 8);
   lb[1] = (uint8_t)(value);

}

uint16_t ruserialize_i16read(const void * buffer)
{
   const uint8_t *lb = buffer;
   return (lb[0] << 8) | lb[1];
}

void     ruserialize_i32write(void * buffer, uint32_t value)
{
   uint8_t *lb = buffer;
   lb[0] = (uint8_t)(value >> 24);
   lb[1] = (uint8_t)(value >> 16);
   lb[2] = (uint8_t)(value >> 8);
   lb[3] = (uint8_t)(value);
}

uint32_t ruserialize_i32read(const void * buffer)
{
   const uint8_t *lb = buffer;
   return (((((lb[0] << 8) | lb[1]) << 8) | lb[2]) << 8) | lb[3];
}

// Referencing https://en.wikipedia.org/wiki/Half-precision_floating-point_format
#define IEEE16_FRACTION_MASK 0x3FF
#define IEEE16_EXPONENT_MASK 0x1F
#define IEEE16_SIGN_MASK     0x1

#define IEEE16_EXPONENT_BIT_OFFSET 10
#define IEEE16_SIGN_BIT_OFFSET     15

#define IEEE16_EXPONENT_ZERO_OFFSET  15
// Note: 0 is a special case
#define IEEE16_MIN_EXPONENT          -14
// Note: 31 is a special case
#define IEEE16_MAX_EXPONENT          15
#define IEEE16_FRACTION              1024.0f // 2^4 (4 = fraction bit length)
void     ruserialize_f16write(void * buffer, float value)
{
   unsigned int computedSignBit;
   int exp;
   int expDiff;
   float range;
   unsigned int ieeeFraction;
   unsigned int ieeeExponent;
   unsigned int ieeeSign;
   uint16_t ieeeValue;
   
   if(signbit(value) == 0)
   {
      computedSignBit = 0;
   }
   else 
   {
      computedSignBit = 1;
   }

   if(isinf(value))
   {
      ieeeFraction = 0;
      ieeeExponent = IEEE16_EXPONENT_MASK;
      ieeeSign = computedSignBit;
   }
   else if(isnan(value))
   {
      ieeeFraction = IEEE16_FRACTION_MASK;
      ieeeExponent = IEEE16_EXPONENT_MASK;
      ieeeSign = 1;
   }
   else if(value == 0)
   {
      ieeeFraction = 0;
      ieeeExponent = 0;
      ieeeSign = computedSignBit;
   }
   else
   {
      range = frexp(value, &exp);
      //printf("frexp: value: %.9g, range: %.9g, exp: %d\n", value, range, exp);
      
      // ABS range
      if(range < 0)
      {
         range *= -1;
      }

      if(exp > IEEE16_MIN_EXPONENT)
      {
         // This is for Normal Numbers
         exp --;
         range = range * 2;
         
         if(exp > IEEE16_MAX_EXPONENT)
         {
            // Round up to infinity
            ieeeFraction = 0;
            ieeeExponent = IEEE16_EXPONENT_MASK;
         }
         else
         {
            ieeeFraction = (int)((range - 1.0f) * IEEE16_FRACTION);
            ieeeExponent = exp + IEEE16_EXPONENT_ZERO_OFFSET;
         }
      }
      else
      {
         // This is for Subnormal Numbers
         if(exp < IEEE16_MIN_EXPONENT)
         {
            expDiff = exp - IEEE16_MIN_EXPONENT;
            range = ldexp(range, expDiff);
            exp = IEEE16_MIN_EXPONENT;
         }
         ieeeFraction = (int)(range * IEEE16_FRACTION);
         ieeeExponent = 0;
      }
      
      //printf("adjusted: range: %.9g, exp: %d\n", range, exp);
      ieeeSign = computedSignBit;
   }
   //printf("ieeeFraction: %d, ieeeExponent: %d, ieeeSign: %d\n", ieeeFraction, ieeeExponent, ieeeSign);
   
   ieeeValue = ((ieeeSign     & IEEE16_SIGN_MASK)     << IEEE16_SIGN_BIT_OFFSET) |
               ((ieeeExponent & IEEE16_EXPONENT_MASK) << IEEE16_EXPONENT_BIT_OFFSET) |
               (ieeeFraction  & IEEE16_FRACTION_MASK);
   //printf("ieeeValue: 0x%.4X\n", ieeeValue);
               
   ruserialize_i16write(buffer, ieeeValue);
   
   //printf("\n\n");
}

float    ruserialize_f16read(const void * buffer)
{
   unsigned int ieeeFraction;
   unsigned int ieeeExponent;
   unsigned int ieeeSign;   
   uint16_t ieeeValue;
   float value;
   float range;

   ieeeValue = ruserialize_i16read(buffer);
   ieeeSign     = (ieeeValue >> IEEE16_SIGN_BIT_OFFSET)     & IEEE16_SIGN_MASK;
   ieeeExponent = (ieeeValue >> IEEE16_EXPONENT_BIT_OFFSET) & IEEE16_EXPONENT_MASK;
   ieeeFraction = ieeeValue                                 & IEEE16_FRACTION_MASK;
   
   if(ieeeExponent == IEEE16_EXPONENT_MASK && ieeeFraction == 0)
   {
      if(ieeeSign == 0)
      {
         return INFINITY;
      }
      else
      {
         return -INFINITY;
      }
   }
   if(ieeeExponent == IEEE16_EXPONENT_MASK && ieeeFraction != 0)
   {
      return NAN;
   }
   if(ieeeExponent == 0 && ieeeFraction == 0)
   {
      if(ieeeSign == 0)
      {
         return 0.0f;
      }
      else
      {
         return -0.0f;
      }
   }
   if(ieeeExponent == 0)
   {
      // Subnormal Numbers
      range = (float)ieeeFraction / IEEE16_FRACTION;
      value = ldexp(range, IEEE16_MIN_EXPONENT);
   }
   else
   {
      // Normalized Numbers
      range = 1.0f + (float)ieeeFraction / IEEE16_FRACTION;
      value = ldexp(range, ieeeExponent - IEEE16_EXPONENT_ZERO_OFFSET);
   }

   if(ieeeSign == 0)
   {
      return value;
   }
   else
   {
      return -value;
   }
}

// Referencing https://en.wikipedia.org/wiki/Single-precision_floating-point_format
#define IEEE32_FRACTION_MASK 0x7FFFFF
#define IEEE32_EXPONENT_MASK 0xFF
#define IEEE32_SIGN_MASK     0x1

#define IEEE32_EXPONENT_BIT_OFFSET 23
#define IEEE32_SIGN_BIT_OFFSET     31

#define IEEE32_EXPONENT_ZERO_OFFSET  127
// Note: 0 is a special case
#define IEEE32_MIN_EXPONENT          -126
// Note: 255 is a special case
#define IEEE32_MAX_EXPONENT          127
#define IEEE32_FRACTION              8388608.0 // 2^23 (23 = fraction bit length)
void     ruserialize_f32write(void * buffer, double value)
{
   unsigned int computedSignBit;
   int exp;
   int expDiff;
   int i;
   double range;
   unsigned int ieeeFraction;
   unsigned int ieeeExponent;
   unsigned int ieeeSign;
   uint32_t ieeeValue;
   
   
   if(signbit(value) == 0)
   {
      computedSignBit = 0;
   }
   else 
   {
      computedSignBit = 1;
   }

   if(value == 0)
   {
      ieeeExponent = 0;
      ieeeFraction = 0;
      ieeeSign = computedSignBit;
   }
   else if(isinf(value))
   {
      ieeeExponent = IEEE32_EXPONENT_MASK;
      ieeeFraction = 0;
      ieeeSign = computedSignBit;
   }
   else if(isnan(value))
   {
      // Using Signal NaN because it felt good.
      ieeeExponent = IEEE32_EXPONENT_MASK;
      ieeeFraction = 1;
      ieeeSign = 1; // High bit is not specified for NAN.
   }
   else
   {
      range = frexp(value, &exp);
      
      // ABS the range
      if(range < 0)
      {
         range *= -1;
      }
      
      //printf("ruserialize_f32write\nfrexp: value: %.9g, range: %.9g, exp: %d\n", value, range, exp);
      
      if(exp > IEEE32_MIN_EXPONENT)
      {
         // This is for Normal Numbers
         // The range is between 0.5 (inclusive) and 1 (exclusive)
         // Transform the range between 1 (inclusive) and 2 (exclusive)
         range = range * 2.0;
         exp --;
         if(exp > IEEE32_MAX_EXPONENT)
         {
            // Number is too large, fit to infinity
            ieeeExponent = IEEE32_EXPONENT_MASK;
            ieeeFraction = 0;
         }
         else
         {
            ieeeExponent = exp + IEEE32_EXPONENT_ZERO_OFFSET;
            ieeeFraction = (int)((range - 1.0f) * IEEE32_FRACTION);
         }
         
      }
      else 
      {
         // This is for Subnormal Numbers
         if(exp < IEEE32_MIN_EXPONENT)
         {
            // Apply Diffrence in scalling
            // This expDiff will be negitive numbers so we will just get really small
            expDiff = exp - IEEE32_MIN_EXPONENT;
            range = ldexp(range, expDiff);
         }
         ieeeExponent = 0;
         ieeeFraction = (int)(range * IEEE32_FRACTION);
      }
      
      //printf("Ajusted Range: %.9g, exp: %d\n", range, (ieeeExponent - IEEE32_EXPONENT_ZERO_OFFSET) );
      ieeeSign = computedSignBit;
   }
   
   //printf("ieeeFraction: %d (0x%X), ieeeExponent: %d\n", ieeeFraction, ieeeFraction, ieeeExponent, ieeeSign );
   ieeeValue = ((ieeeSign     & IEEE32_SIGN_MASK)     << IEEE32_SIGN_BIT_OFFSET) |
               ((ieeeExponent & IEEE32_EXPONENT_MASK) << IEEE32_EXPONENT_BIT_OFFSET) |
               (ieeeFraction  & IEEE32_FRACTION_MASK);
   //printf("ieeeValue: 0x%.8X\n", ieeeValue);
               
   ruserialize_i32write(buffer, ieeeValue);
   //printf("\n\n");
}

double   ruserialize_f32read(const void * buffer)
{
   unsigned int ieeeFraction;
   unsigned int ieeeExponent;
   unsigned int ieeeSign;
   uint32_t ieeeValue;
   float value;
   float range;

   ieeeValue = ruserialize_i32read(buffer);
   ieeeSign     = (ieeeValue >> IEEE32_SIGN_BIT_OFFSET)     & IEEE32_SIGN_MASK;
   ieeeExponent = (ieeeValue >> IEEE32_EXPONENT_BIT_OFFSET) & IEEE32_EXPONENT_MASK;
   ieeeFraction = ieeeValue                                 & IEEE32_FRACTION_MASK;
   
   if(ieeeExponent == IEEE32_EXPONENT_MASK && ieeeFraction == 0)
   {
      if(ieeeSign == 0)
      {
         return INFINITY;
      }
      else
      {
         return -INFINITY;
      }
   }
   if(ieeeExponent == IEEE32_EXPONENT_MASK && ieeeFraction != 0)
   {
      return NAN;
   }
   if(ieeeExponent == 0 && ieeeFraction == 0)
   {
      if(ieeeSign == 0)
      {
         return 0.0f;
      }
      else
      {
         return -0.0f;
      }
   }
   if(ieeeExponent == 0)
   {
      // Subnormal Numbers
      range = (float)ieeeFraction / IEEE16_FRACTION;
      value = ldexp(range, IEEE32_MIN_EXPONENT);
   }
   else
   {
      // Normalized Numbers
      range = 1.0f + (float)ieeeFraction / IEEE32_FRACTION;
      value = ldexp(range, ieeeExponent - IEEE32_EXPONENT_ZERO_OFFSET);
   }

   if(ieeeSign == 0)
   {
      return value;
   }
   else
   {
      return -value;
   }
}



