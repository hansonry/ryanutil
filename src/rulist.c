#include <stdlib.h>
#include <string.h>
#include <ruarray.h>
#include <rulist.h>

#include "memunittest.h"

#define DEFAULT_GROWBY 32

void rulist_init(struct rulist * list, void ** basePtr, size_t elementSize, size_t growBy)
{
   if(growBy == 0)
   {
      list->growBy = DEFAULT_GROWBY;
   }
   else
   {
      list->growBy = growBy;
   }
   
   rumem_init(&list->mem, basePtr, elementSize, list->growBy);
   list->count = 0;
}

void rulist_destroy(struct rulist * list)
{
   rumem_destroy(&list->mem);
   list->count  = 0;
   list->growBy = 0;
}


void * rulist_add(struct rulist * list, size_t * newIndex)
{
   size_t index;
   size_t newSize;
   
   if(list->count >= list->mem.size)
   {
      newSize = list->count + list->growBy;
      rumem_growto(&list->mem, newSize);
   }
   index = list->count;
   list->count ++;
   if(newIndex != NULL)
   {
      *newIndex = index;
   }
   return rumem_get(&list->mem, index);
}

void * rulist_addcopy(struct rulist * list, void * source, size_t * newIndex)
{
   void * newAddress;
   newAddress = rulist_add(list, newIndex);
   memcpy(newAddress, source, list->mem.elementSize);
   return newAddress;
}

enum rustatus rulist_removefast(struct rulist * list, size_t index)
{
   
   size_t newCount;
   if(index >= list->count)
   {
      return e_rus_failure;
   }
   
   newCount = list->count - 1;
   if(index < newCount)
   {
      (void)rulist_swap(list, index, newCount);
   }
   
   list->count = newCount;
   return e_rus_success;
}

enum rustatus rulist_removeordered(struct rulist * list, size_t index)
{
   size_t newCount;
   if(index >= list->count)
   {
      return e_rus_failure;
   }
   newCount = list->count - 1;

   if(index < newCount)
   {
      (void)rulist_blockcopy(list, index, index + 1, list->count - index - 1);
   }
   list->count = newCount;
   return e_rus_success;
}

enum rustatus rulist_swap(struct rulist * list, size_t index1, size_t index2)
{
   return rumem_swap(&list->mem, index1, index2);
}


size_t rulist_blockcopy(struct rulist * list, size_t toIndex, size_t fromIndex, size_t count)
{
   // Saturate

   if(toIndex   >= list->count ||
      fromIndex >= list->count)
   {
      return 0;
   }

   
   // Saturate count
   if(fromIndex + count > list->count)
   {
      count = list->count - fromIndex;
   }
   if(toIndex + count > list->count)
   {
      count = list->count - toIndex;
   }

   return rumem_blockcopy(&list->mem, toIndex, fromIndex, count);
}

void rulist_clear(struct rulist * list)
{
   list->count = 0;
}

void * rulist_insert(struct rulist * list, size_t index)
{
   size_t newSize;

   if(index > list->count)
   {
      return NULL;
   }

   if(list->count >= list->mem.size)
   {
      newSize = list->count + list->growBy;
      rumem_growto(&list->mem, newSize);
   }
   if(index < list->count)
   {
      (void)rumem_blockcopy(&list->mem, index + 1, index, list->count - index);
   }
   list->count++; 
   return rumem_get(&list->mem, index);
}

enum rustatus rulist_copyin(struct rulist * list, size_t index, const void * data)
{
   if(index >= list->count)
   {
      return e_rus_failure;
   }
   return rumem_copyin(&list->mem, index, data);
}

enum rustatus rulist_copyout(struct rulist * list, void * data, size_t index)
{
   if(index >= list->count)
   {
      return e_rus_failure;
   }
   return rumem_copyout(&list->mem, data, index);
}

size_t rulist_copyinto(struct rulist * list, const void * data, size_t index, size_t size)
{
   if(index >= list->count)
   {
      return 0;
   }
   if(index + size > list->count)
   {
      size = list->count - index;
   }
   return rumem_copyinto(&list->mem, data, index, size);
}

void rulist_copyinall(struct rulist * list, const void * data)
{
   if(list->count > 0)
   {
      (void)rumem_copyinto(&list->mem, data, 0, list->count);
   }
}

size_t rulist_indexof(struct rulist * list, const void * address)
{
   size_t index;
   index = rumem_indexof(&list->mem, address);
   if(index >= list->count)
   {
      index = list->count;
   }
   return index;
}

size_t rulist_count(struct rulist * list)
{
   return list->count;
}

