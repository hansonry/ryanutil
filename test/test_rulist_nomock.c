#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <stdbool.h>
#include "rulist.h"


struct thing
{
   int x;
   int y;
   int z;
};
static void util_set(struct thing * t, int x, int y, int z)
{
   t->x = x;
   t->y = y;
   t->z = z;
}

static void util_write(struct thing * thingbase, size_t count)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      util_set(&thingbase[i], i, i * -11, i + 4);
   }
}

static void util_verify_order(struct thing * thingbase, size_t count, size_t * order)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      //printf("verify %i = %i\n", (int)i, (int)order[i]);
      assert_int_equal(order[i],       thingbase[i].x);
      assert_int_equal(order[i] * -11, thingbase[i].y);
      assert_int_equal(order[i] + 4,   thingbase[i].z);
   }
}

static void util_set_order(size_t * order, size_t arraySize, size_t toIndex, size_t fromIndex, size_t count)
{
   int i;
   // Init the order
   for(i = 0; i < arraySize; i++)
   {
      order[i] = i;
   }
   
   for(i = 0; i < count; i++)
   {
      if(toIndex + i < arraySize && 
         fromIndex + i < arraySize)
      {
         order[toIndex + i] = fromIndex + i;
      }
   }
   //for(i = 0; i < arraySize; i++) printf("o %i = %i\n", (int)i, (int)order[i]);
}   




static void test_rulist_initm(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   
   rulist_initm(&listdata, list, 0);
   
   
   

   rulist_destroy(&listdata);
}

static void test_rulist_add_once(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newthing;

   rulist_initm(&listdata, list, 0);


   newthing = rulist_add(&listdata, NULL);
   util_set(newthing, 1, 2, 3);
   
   assert_int_equal(1, list[0].x);
   assert_int_equal(2, list[0].y);
   assert_int_equal(3, list[0].z);
   assert_int_equal(3, list[0].z);
   
   
   rulist_destroy(&listdata);
}



static void test_rulist_add_many(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newthing;
   size_t newIndex;
   int i;

   rulist_initm(&listdata, list, 4);

   for(i = 0; i < 100; i ++)
   {
      size_t * index_ptr;

      if(i % 3 == 0)
      {
         index_ptr = NULL;
      }
      else
      {
         index_ptr = &newIndex;
      }
      newthing = rulist_add(&listdata, index_ptr);
      
      if(index_ptr != NULL)
      {
         assert_int_equal(i, newIndex);
      }
      
      util_set(newthing, i, i, i);
   }
   
   for(i = 0; i < 100; i++)
   {
      assert_int_equal(i, list[i].x);
      assert_int_equal(i, list[i].y);
      assert_int_equal(i, list[i].z);
   }
   rulist_destroy(&listdata);
}

static void util_addthings(struct rulist * listdata, int count)
{
   int i;
   struct thing * newthing;

   for(i = 0; i < count; i++)
   {
      newthing = rulist_add(listdata, NULL);
      util_set(newthing, i, i, i);
   }
   assert_int_equal(listdata->count, count);
}

static void test_rulist_removefast(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newthing;

   rulist_initm(&listdata, list, 0);
   
   assert_int_equal(rulist_removefast(&listdata, 0), e_rus_failure);

   util_addthings(&listdata, 5);
   assert_int_equal(rulist_removefast(&listdata, 5), e_rus_failure);
   assert_int_equal(rulist_removefast(&listdata, 0), e_rus_success);
   assert_int_equal(listdata.count, 4);
   assert_int_equal(list[0].x, 4);

   
   assert_int_equal(rulist_removefast(&listdata, 1), e_rus_success);
   assert_int_equal(listdata.count, 3);
   assert_int_equal(list[1].x, 3);

   assert_int_equal(rulist_removefast(&listdata, 0), e_rus_success);
   assert_int_equal(listdata.count, 2);
   assert_int_equal(list[0].x, 2);

   rulist_destroy(&listdata);
}

static void test_rulist_removeordered(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newthing;

   rulist_initm(&listdata, list, 0);
   
   assert_int_equal(rulist_removeordered(&listdata, 0), e_rus_failure);

   util_addthings(&listdata, 5);
   assert_int_equal(rulist_removeordered(&listdata, 5), e_rus_failure);
   assert_int_equal(rulist_removeordered(&listdata, 0), e_rus_success);
   assert_int_equal(listdata.count, 4);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[1].x, 2);
   assert_int_equal(list[2].x, 3);
   assert_int_equal(list[3].x, 4);

   
   assert_int_equal(rulist_removeordered(&listdata, 1), e_rus_success);
   assert_int_equal(listdata.count, 3);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[1].x, 3);
   assert_int_equal(list[2].x, 4);

   assert_int_equal(rulist_removeordered(&listdata, 0), e_rus_success);
   assert_int_equal(listdata.count, 2);
   assert_int_equal(list[0].x, 3);
   assert_int_equal(list[1].x, 4);

   rulist_destroy(&listdata);
}

static void test_rulist_addcopy(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing  newthing;
   size_t index;

   rulist_initm(&listdata, list, 0);
   
   util_set(&newthing, 1, 2, 3);
   assert_ptr_equal(rulist_addcopy(&listdata, &newthing, NULL), &list[0]); 
   assert_int_equal(listdata.count, 1);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[0].y, 2);
   assert_int_equal(list[0].z, 3);

   util_set(&newthing, 4, 5, 6);
   assert_ptr_equal(rulist_addcopy(&listdata, &newthing, &index), &list[1]); 
   assert_int_equal(listdata.count, 2);
   assert_int_equal(list[1].x, 4);
   assert_int_equal(list[1].y, 5);
   assert_int_equal(list[1].z, 6);
   assert_int_equal(index, 1);


   rulist_removefast(&listdata, 0);

   util_set(&newthing, -4, -5, -6);
   assert_ptr_equal(rulist_addcopy(&listdata, &newthing, &index), &list[1]); 
   assert_int_equal(listdata.count, 2);
   assert_int_equal(list[1].x, -4);
   assert_int_equal(list[1].y, -5);
   assert_int_equal(list[1].z, -6);
   assert_int_equal(index, 1);


   rulist_destroy(&listdata);
}

static void test_rulist_clear(void **state)
{
   struct thing * list;
   struct rulist listdata;

   rulist_initm(&listdata, list, 0);

   rulist_clear(&listdata);
   assert_int_equal(listdata.count, 0);

   (void)rulist_add(&listdata, NULL);
   (void)rulist_add(&listdata, NULL);
   assert_int_equal(listdata.count, 2);
   
   rulist_clear(&listdata);
   assert_int_equal(listdata.count, 0);

   rulist_destroy(&listdata);
}

static void test_rulist_insert(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newThing;

   rulist_initm(&listdata, list, 0);

   newThing = rulist_insert(&listdata, 0);
   util_set(newThing, 0, 0, 0);
   assert_int_equal(list[0].x, 0);
   assert_ptr_equal(newThing, &list[0]);

   newThing = rulist_insert(&listdata, 0);
   util_set(newThing, 1, 1, 1);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[1].x, 0);
   assert_ptr_equal(newThing, &list[0]);
   
   newThing = rulist_insert(&listdata, 2);
   util_set(newThing, 2, 2, 2);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[1].x, 0);
   assert_int_equal(list[2].x, 2);
   assert_ptr_equal(newThing, &list[2]);

   newThing = rulist_insert(&listdata, 2);
   util_set(newThing, 3, 3, 3);
   assert_int_equal(list[0].x, 1);
   assert_int_equal(list[1].x, 0);
   assert_int_equal(list[2].x, 3);
   assert_int_equal(list[3].x, 2);
   assert_ptr_equal(newThing, &list[2]);

   newThing = rulist_insert(&listdata, 0);
   util_set(newThing, 4, 4, 4);
   assert_int_equal(list[0].x, 4);
   assert_int_equal(list[1].x, 1);
   assert_int_equal(list[2].x, 0);
   assert_int_equal(list[3].x, 3);
   assert_int_equal(list[4].x, 2);
   assert_ptr_equal(newThing, &list[0]);

   newThing = rulist_insert(&listdata, 7);
   assert_ptr_equal(newThing, NULL);
   
   rulist_destroy(&listdata);
}

int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_rulist_initm),
      cmocka_unit_test(test_rulist_add_once),
      cmocka_unit_test(test_rulist_add_many),
      cmocka_unit_test(test_rulist_removefast),
      cmocka_unit_test(test_rulist_removeordered),
      cmocka_unit_test(test_rulist_addcopy),
      cmocka_unit_test(test_rulist_clear),
      cmocka_unit_test(test_rulist_insert),
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


