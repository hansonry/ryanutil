#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <stdbool.h>
#include "rulist.h"


struct thing
{
   int x;
   int y;
   int z;
};
static void util_set(struct thing * t, int x, int y, int z)
{
   t->x = x;
   t->y = y;
   t->z = z;
}

static void util_write(struct thing * thingbase, size_t count)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      util_set(&thingbase[i], i, i * -11, i + 4);
   }
}

static void util_verify_order(struct thing * thingbase, size_t count, size_t * order)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      //printf("verify %i = %i\n", (int)i, (int)order[i]);
      assert_int_equal(order[i],       thingbase[i].x);
      assert_int_equal(order[i] * -11, thingbase[i].y);
      assert_int_equal(order[i] + 4,   thingbase[i].z);
   }
}

static void util_set_order(size_t * order, size_t arraySize, size_t toIndex, size_t fromIndex, size_t count)
{
   int i;
   // Init the order
   for(i = 0; i < arraySize; i++)
   {
      order[i] = i;
   }
   
   for(i = 0; i < count; i++)
   {
      if(toIndex + i < arraySize && 
         fromIndex + i < arraySize)
      {
         order[toIndex + i] = fromIndex + i;
      }
   }
   //for(i = 0; i < arraySize; i++) printf("o %i = %i\n", (int)i, (int)order[i]);
}   

#define util_rulist_initm(list, base, growBy)  util_rulist_init((list), (void**)&(base), sizeof(*(base)), (growBy))
static void util_rulist_init(struct rulist * list, void ** basePtr, size_t elementSize, size_t growBy)
{
   size_t expected_growBy;
   if(growBy == 0)
   {
      expected_growBy = 32;
   }
   else
   {
      expected_growBy = growBy;
   }
   
   
   expect_function_call(rumem_init);
   expect_value(rumem_init, mem, &list->mem);
   expect_value(rumem_init, basePtr, basePtr);
   expect_value(rumem_init, elementSize, elementSize);
   expect_value(rumem_init, initSize, expected_growBy);
   
   rulist_init(list, basePtr, elementSize, growBy);
   
   assert_int_equal(list->growBy,     expected_growBy);
   assert_int_equal(list->count,      0);
   
   list->mem.size = expected_growBy;
   list->mem.elementSize = elementSize;

}

static void util_rulist_destroy(struct rulist * list)
{
   expect_function_call(rumem_destroy);
   expect_value(rumem_destroy, mem, &list->mem);

   rulist_destroy(list);
   assert_int_equal(list->count,      0);
   assert_int_equal(list->growBy,     0);
}


static void test_rulist_initm(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   
   expect_function_call(rumem_init);
   expect_value(rumem_init, mem, &listdata.mem);
   expect_value(rumem_init, basePtr, &list);
   expect_value(rumem_init, elementSize, sizeof(struct thing));
   expect_value(rumem_init, initSize, 32);
   
   rulist_initm(&listdata, list, 0);
   
   assert_int_equal(listdata.growBy, 32);
   assert_int_equal(listdata.count,  0);
   
   
   expect_function_call(rumem_destroy);
   expect_value(rumem_destroy, mem, &listdata.mem);

   rulist_destroy(&listdata);
}

static void util_expect_rumem_get(struct rumem * mem, size_t index, void * retaddress)
{
   expect_function_call( rumem_get);
   expect_value(         rumem_get, mem,   mem);
   expect_value(         rumem_get, index, index);
   will_return(          rumem_get, retaddress);
}
 


static void util_expect_swap(struct rulist * list, size_t index1, size_t index2, enum rustatus status)
{
   expect_function_call( rumem_swap );
   expect_value(         rumem_swap, mem,    &list->mem);
   expect_value(         rumem_swap, index1, index1);
   expect_value(         rumem_swap, index2, index2);
   will_return(          rumem_swap, status);
   
}

static void* util_rulist_add(struct rulist * list, size_t * newIndex, int expected_new_index, void * expected_new_address, size_t expected_new_size)
{
   struct thing * newthing;
   
   if(expected_new_size != 0)
   {
      expect_function_call( rumem_growto );
      expect_value(         rumem_growto, mem,        &list->mem);
      expect_value(         rumem_growto, targetSize, expected_new_size);
   }
   
   util_expect_rumem_get(&list->mem, expected_new_index, expected_new_address);
   newthing = rulist_add(list, newIndex);
   assert_ptr_equal(expected_new_address, newthing);
   assert_int_equal(expected_new_index + 1, list->count);
   if(newIndex != NULL)
   {
      assert_int_equal(expected_new_index, *newIndex);
   }
   
   return newthing;

}


static void test_rulist_swap(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   
   util_expect_swap(&listdata, 0, 1, e_rus_success);
   assert_int_equal(e_rus_success, rulist_swap(&listdata, 0, 1));   

   util_rulist_add(&listdata, NULL, 2, &list[2], 0);

   util_expect_swap(&listdata, 1, 2, e_rus_success);
   assert_int_equal(e_rus_success, rulist_swap(&listdata, 1, 2));   
   
   util_rulist_destroy(&listdata);
}


static void test_rulist_swap_outofrange(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   
   util_expect_swap(&listdata, 0, 1, e_rus_failure);
   assert_int_equal(e_rus_failure, rulist_swap(&listdata, 0, 1));   

   util_rulist_add(&listdata, NULL, 0, &list[0], 0);

   util_expect_swap(&listdata, 1, 2, e_rus_failure);
   assert_int_equal(e_rus_failure, rulist_swap(&listdata, 1, 2));   
   
   util_rulist_destroy(&listdata);
}



static void test_rulist_blockcopy(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);

   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   util_rulist_add(&listdata, NULL, 2, &list[2], 0);
   util_rulist_add(&listdata, NULL, 3, &list[3], 0);
   util_rulist_add(&listdata, NULL, 4, &list[4], 0);
   
   expect_function_call( rumem_blockcopy );
   expect_value(         rumem_blockcopy, mem,       &listdata.mem );
   expect_value(         rumem_blockcopy, toIndex,   0 );
   expect_value(         rumem_blockcopy, fromIndex, 1 );
   expect_value(         rumem_blockcopy, count,     2 );
   will_return(          rumem_blockcopy, 2 );
   assert_int_equal(rulist_blockcopy(&listdata, 0, 1, 2), 2);

   expect_function_call( rumem_blockcopy );
   expect_value(         rumem_blockcopy, mem,       &listdata.mem );
   expect_value(         rumem_blockcopy, toIndex,   0 );
   expect_value(         rumem_blockcopy, fromIndex, 3 );
   expect_value(         rumem_blockcopy, count,     2 );
   will_return(          rumem_blockcopy, 2 );
   assert_int_equal(rulist_blockcopy(&listdata, 0, 3, 5), 2);
   
   expect_function_call( rumem_blockcopy );
   expect_value(         rumem_blockcopy, mem,       &listdata.mem );
   expect_value(         rumem_blockcopy, toIndex,   3 );
   expect_value(         rumem_blockcopy, fromIndex, 0 );
   expect_value(         rumem_blockcopy, count,     2 );
   will_return(          rumem_blockcopy, 2 );
   assert_int_equal(rulist_blockcopy(&listdata, 3, 0, 5), 2);
   
   
   assert_int_equal(rulist_blockcopy(&listdata, 5, 0, 1), 0);
   assert_int_equal(rulist_blockcopy(&listdata, 0, 5, 1), 0);
   
   util_rulist_destroy(&listdata);
}

static void test_rulist_add_once(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing * newthing;
   size_t newIndex;

   util_rulist_initm(&listdata, list, 0);


   expect_function_call( rumem_get );
   expect_value(         rumem_get, mem,   &listdata.mem );
   expect_value(         rumem_get, index, 0 );
   will_return(          rumem_get, &list[0] );
   newthing = rulist_add(&listdata, &newIndex );
   
   assert_ptr_equal(&list[0], newthing);
   assert_int_equal(1, listdata.count);
   assert_int_equal(0, newIndex);
   
   util_rulist_destroy(&listdata);
}


static void test_rulist_add_many(void **state)
{
   struct thing * list;
   struct rulist listdata;
   size_t newIndex;
   int i;

   util_rulist_initm(&listdata, list, 4);

   for(i = 0; i < 100; i ++)
   {
      size_t expected_new_size;
      size_t * index_ptr;
      if(i % 4 == 0 && i != 0)
      {
         expected_new_size = i + 4;
      }
      else
      {
         expected_new_size = 0;
      }
      if(i % 3 == 0)
      {
         index_ptr = NULL;
      }
      else
      {
         index_ptr = &newIndex;
      }
      util_rulist_add(&listdata, &newIndex, i, &list[i], expected_new_size);
      
      if(expected_new_size != 0)
      {
         listdata.mem.size += 4;
      }
   }
   
   util_rulist_destroy(&listdata);
}



static void test_rulist_removefast(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   
   assert_int_equal(e_rus_success, rulist_removefast(&listdata, 0));   
   assert_int_equal(0, listdata.count);
   
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);

   util_expect_swap(&listdata, 0, 1, e_rus_success);
   assert_int_equal(e_rus_success, rulist_removefast(&listdata, 0));   
   assert_int_equal(1, listdata.count);

   assert_int_equal(e_rus_success, rulist_removefast(&listdata, 0));   
   assert_int_equal(0, listdata.count);

   
   util_rulist_destroy(&listdata);
}

static void test_rulist_removefast_outofrange(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   
   assert_int_equal(e_rus_failure, rulist_removefast(&listdata, 0));   
   assert_int_equal(0, listdata.count);

   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);

   assert_int_equal(e_rus_failure, rulist_removefast(&listdata, 2));   
   assert_int_equal(2, listdata.count);
   
   util_rulist_destroy(&listdata);
}

static void util_expect_blockcopy(struct rulist * listdata, size_t toIndex, size_t fromIndex, size_t count, size_t returnValue)
{
   expect_function_call( rumem_blockcopy );
   expect_value(         rumem_blockcopy, mem,       &listdata->mem );
   expect_value(         rumem_blockcopy, toIndex,   toIndex );
   expect_value(         rumem_blockcopy, fromIndex, fromIndex );
   expect_value(         rumem_blockcopy, count,     count );
   will_return(          rumem_blockcopy, returnValue );
}

static void test_rulist_removeordered(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   
   assert_int_equal(e_rus_success, rulist_removeordered(&listdata, 0));   
   assert_int_equal(0, listdata.count);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);

   util_expect_blockcopy(&listdata, 0, 1, 1, 1);
   assert_int_equal(e_rus_success, rulist_removeordered(&listdata, 0));   
   assert_int_equal(1, listdata.count);

   assert_int_equal(e_rus_success, rulist_removeordered(&listdata, 0));   
   assert_int_equal(0, listdata.count);
   util_rulist_destroy(&listdata);
}

static void test_rulist_removeordered_outofrange(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 0);
   
   
   assert_int_equal(e_rus_failure, rulist_removeordered(&listdata, 0));   
   assert_int_equal(0, listdata.count);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);

   assert_int_equal(e_rus_failure, rulist_removeordered(&listdata, 2));   
   assert_int_equal(2, listdata.count);

   util_rulist_destroy(&listdata);
}

static void test_rulist_addcopy(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing newThing;
   struct thing savedThing;
   size_t index;
   
   util_rulist_initm(&listdata, list, 0);
  
   util_set(&newThing,   1, 2, 4);
   util_set(&savedThing, 0, 0, 0);

   util_expect_rumem_get(&listdata.mem, 0, &savedThing); 
   assert_ptr_equal(rulist_addcopy(&listdata, &newThing, NULL), &savedThing);
   
   assert_int_equal(newThing.x, savedThing.x);
   assert_int_equal(newThing.y, savedThing.y);
   assert_int_equal(newThing.z, savedThing.z);

   assert_int_equal(1, listdata.count);

   util_set(&newThing,   2, 1, 5);
   util_expect_rumem_get(&listdata.mem, 1, &savedThing); 
   assert_ptr_equal(rulist_addcopy(&listdata, &newThing, &index), &savedThing);
   assert_int_equal(newThing.x, savedThing.x);
   assert_int_equal(newThing.y, savedThing.y);
   assert_int_equal(newThing.z, savedThing.z);

   assert_int_equal(2, listdata.count);
   assert_int_equal(1, index);


   util_rulist_destroy(&listdata);
}

static void test_rulist_insert_once(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 8);
   listdata.mem.size = 8;

   util_expect_rumem_get(&listdata.mem, 0, &list[0]);  
   assert_int_equal(rulist_insert(&listdata, 0), &list[0]);
   assert_int_equal(listdata.count, 1);

   util_expect_blockcopy(&listdata, 1, 0, 1, 1);
   util_expect_rumem_get(&listdata.mem, 0, &list[0]);  
   assert_int_equal(rulist_insert(&listdata, 0), &list[0]);
   assert_int_equal(listdata.count, 2);

   util_expect_blockcopy(&listdata, 1, 0, 2, 1);
   util_expect_rumem_get(&listdata.mem, 0, &list[0]);  
   assert_int_equal(rulist_insert(&listdata, 0), &list[0]);
   assert_int_equal(listdata.count, 3);
   
   util_expect_blockcopy(&listdata, 2, 1, 2, 1);
   util_expect_rumem_get(&listdata.mem, 1, &list[1]);  
   assert_int_equal(rulist_insert(&listdata, 1), &list[1]);
   assert_int_equal(listdata.count, 4);

   util_expect_rumem_get(&listdata.mem, 4, &list[4]);  
   assert_int_equal(rulist_insert(&listdata, 4), &list[4]);
   assert_int_equal(listdata.count, 5);
   
   assert_int_equal(rulist_insert(&listdata, 6), NULL);
   assert_int_equal(listdata.count, 5);

   util_rulist_destroy(&listdata);
}

static void test_rulist_insert_many(void **state)
{
   struct thing * list;
   struct rulist listdata;
   int i;
   size_t newSize;
   
   util_rulist_initm(&listdata, list, 4);
   listdata.mem.size = 4;
   newSize = 4;

   for(i = 0; i < 100; i++)
   {
      if(i > 0)
      {
         if(i % 4 == 0)
         {
            newSize += 4;
            expect_function_call( rumem_growto );
            expect_value(         rumem_growto, mem,        &listdata.mem);
            expect_value(         rumem_growto, targetSize, newSize);
         }
         util_expect_blockcopy(&listdata, 1, 0, i, 1);
      }
      util_expect_rumem_get(&listdata.mem, 0, &list[0]);  
      assert_int_equal(rulist_insert(&listdata, 0), &list[0]);
      assert_int_equal(listdata.count, i + 1);

      listdata.mem.size = newSize;
   }

   util_rulist_destroy(&listdata);
}

static void test_rulist_copyin(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing data;
   
   util_rulist_initm(&listdata, list, 8);
   
   assert_int_equal(rulist_copyin(&listdata, 0, &data), e_rus_failure);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   util_rulist_add(&listdata, NULL, 2, &list[2], 0);

   expect_function_call( rumem_copyin );
   expect_value(         rumem_copyin, mem,   &listdata.mem);
   expect_value(         rumem_copyin, index, 0);
   expect_value(         rumem_copyin, data,  &data);
   will_return(          rumem_copyin,        e_rus_success);

   assert_int_equal(rulist_copyin(&listdata, 0, &data), e_rus_success);


   expect_function_call( rumem_copyin );
   expect_value(         rumem_copyin, mem,   &listdata.mem);
   expect_value(         rumem_copyin, index, 2);
   expect_value(         rumem_copyin, data,  &data);
   will_return(          rumem_copyin,        e_rus_success);

   assert_int_equal(rulist_copyin(&listdata, 2, &data), e_rus_success);


   assert_int_equal(rulist_copyin(&listdata, 3, &data), e_rus_failure);

   expect_function_call( rumem_copyin );
   expect_value(         rumem_copyin, mem,   &listdata.mem);
   expect_value(         rumem_copyin, index, 1);
   expect_value(         rumem_copyin, data,  NULL);
   will_return(          rumem_copyin,        e_rus_failure);

   assert_int_equal(rulist_copyin(&listdata, 1, NULL), e_rus_failure);

   
   util_rulist_destroy(&listdata);

}

static void test_rulist_copyout(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing data;
   
   util_rulist_initm(&listdata, list, 8);
   
   assert_int_equal(rulist_copyout(&listdata, &data, 0), e_rus_failure);
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   util_rulist_add(&listdata, NULL, 2, &list[2], 0);

   expect_function_call( rumem_copyout );
   expect_value(         rumem_copyout, mem,   &listdata.mem);
   expect_value(         rumem_copyout, index, 0);
   expect_value(         rumem_copyout, data,  &data);
   will_return(          rumem_copyout,        e_rus_success);

   assert_int_equal(rulist_copyout(&listdata, &data, 0), e_rus_success);

   expect_function_call( rumem_copyout );
   expect_value(         rumem_copyout, mem,   &listdata.mem);
   expect_value(         rumem_copyout, index, 2);
   expect_value(         rumem_copyout, data,  &data);
   will_return(          rumem_copyout,        e_rus_success);

   assert_int_equal(rulist_copyout(&listdata, &data, 2), e_rus_success);

   assert_int_equal(rulist_copyout(&listdata, &data, 3), e_rus_failure);

   expect_function_call( rumem_copyout );
   expect_value(         rumem_copyout, mem,   &listdata.mem);
   expect_value(         rumem_copyout, index, 1);
   expect_value(         rumem_copyout, data,  NULL);
   will_return(          rumem_copyout,        e_rus_failure);

   assert_int_equal(rulist_copyout(&listdata, NULL, 1), e_rus_failure);
   
   util_rulist_destroy(&listdata);

}

static void test_rulist_indexof(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 8);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[0]);
   will_return(          rumem_indexof,          8);

   assert_int_equal(rulist_indexof(&listdata, &list[0]), 0);

   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   util_rulist_add(&listdata, NULL, 2, &list[2], 0);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[0]);
   will_return(          rumem_indexof,          0);

   assert_int_equal(rulist_indexof(&listdata, &list[0]), 0);
   
   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[1]);
   will_return(          rumem_indexof,          1);
   
   assert_int_equal(rulist_indexof(&listdata, &list[1]), 1);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[2]);
   will_return(          rumem_indexof,          2);
   
   assert_int_equal(rulist_indexof(&listdata, &list[2]), 2);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[3]);
   will_return(          rumem_indexof,          3);
   
   assert_int_equal(rulist_indexof(&listdata, &list[3]), 3);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[4]);
   will_return(          rumem_indexof,          4);
   
   assert_int_equal(rulist_indexof(&listdata, &list[4]), 3);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[8]);
   will_return(          rumem_indexof,          8);
   
   assert_int_equal(rulist_indexof(&listdata, &list[8]), 3);

   util_rulist_add(&listdata, NULL, 3, &list[3], 0);

   expect_function_call( rumem_indexof );
   expect_value(         rumem_indexof, mem,     &listdata.mem);
   expect_value(         rumem_indexof, address, &list[8]);
   will_return(          rumem_indexof,          8);
   
   assert_int_equal(rulist_indexof(&listdata, &list[8]), 4);

   util_rulist_destroy(&listdata);

}


static void test_rulist_count(void **state)
{
   struct thing * list;
   struct rulist listdata;
   
   util_rulist_initm(&listdata, list, 8);

   assert_int_equal(rulist_count(&listdata), 0);

   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   util_rulist_add(&listdata, NULL, 2, &list[2], 0);

   assert_int_equal(rulist_count(&listdata), 3);

   util_rulist_add(&listdata, NULL, 3, &list[3], 0);
   util_rulist_add(&listdata, NULL, 4, &list[4], 0);
   util_rulist_add(&listdata, NULL, 5, &list[5], 0);
   
   assert_int_equal(rulist_count(&listdata), 6);

   util_rulist_destroy(&listdata);

}

static void test_rulist_copyinto(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing data;
   
   util_rulist_initm(&listdata, list, 8);
   
   assert_int_equal(rulist_copyinto(&listdata, &data, 0, 1), 0);
   
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   
   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &data);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  1);
   will_return(          rumem_copyinto,        1);

   assert_int_equal(rulist_copyinto(&listdata, &data, 0, 1), 1);

   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &data);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  1);
   will_return(          rumem_copyinto,        1);

   assert_int_equal(rulist_copyinto(&listdata, &data, 0, 2), 1);

   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  NULL);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  1);
   will_return(          rumem_copyinto,        0);

   assert_int_equal(rulist_copyinto(&listdata, NULL, 0, 1), 0);

   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &listdata);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  1);
   will_return(          rumem_copyinto,        0);

   assert_int_equal(rulist_copyinto(&listdata, &listdata, 0, 1), 0);
   
   util_rulist_destroy(&listdata);
}


static void test_rulist_copyinall(void **state)
{
   struct thing * list;
   struct rulist listdata;
   struct thing data;
   
   util_rulist_initm(&listdata, list, 8);
   
   rulist_copyinall(&listdata, &data);
   
   
   util_rulist_add(&listdata, NULL, 0, &list[0], 0);
   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &data);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  1);
   will_return(          rumem_copyinto,        1);
   
   rulist_copyinall(&listdata, &data);

   util_rulist_add(&listdata, NULL, 1, &list[1], 0);
   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &data);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  2);
   will_return(          rumem_copyinto,        2);
   
   rulist_copyinall(&listdata, &data);


   expect_function_call( rumem_copyinto );
   expect_value(         rumem_copyinto, mem,   &listdata.mem);
   expect_value(         rumem_copyinto, data,  &listdata);
   expect_value(         rumem_copyinto, index, 0);
   expect_value(         rumem_copyinto, size,  2);
   will_return(          rumem_copyinto,        2);
   
   rulist_copyinall(&listdata, &listdata);

   
   util_rulist_destroy(&listdata);
}


int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_rulist_initm),
      cmocka_unit_test(test_rulist_swap), 
      cmocka_unit_test(test_rulist_swap_outofrange),
      cmocka_unit_test(test_rulist_blockcopy),
      cmocka_unit_test(test_rulist_add_once),      
      cmocka_unit_test(test_rulist_add_many),
      cmocka_unit_test(test_rulist_removefast),
      cmocka_unit_test(test_rulist_removefast_outofrange), 
      cmocka_unit_test(test_rulist_removeordered),
      cmocka_unit_test(test_rulist_removeordered_outofrange), 
      cmocka_unit_test(test_rulist_addcopy), 
      cmocka_unit_test(test_rulist_insert_once), 
      cmocka_unit_test(test_rulist_insert_many), 
      cmocka_unit_test(test_rulist_copyin), 
      cmocka_unit_test(test_rulist_copyout), 
      cmocka_unit_test(test_rulist_indexof), 
      cmocka_unit_test(test_rulist_count), 
      cmocka_unit_test(test_rulist_copyinto), 
      cmocka_unit_test(test_rulist_copyinall), 
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


