
function BuildAndRunTest(testName, settings, linkto, testSourcs)
   
   testObjects = Compile(settings, testSourcs);
   exe = Link(settings, testName,  testObjects, linkto)

   if family == "windows" then
      test_exe_cmd = exe:gsub("/", "\\")
   else
      test_exe_cmd = "./" .. exe
   end
   
   output = "testout." .. testName .. ".temp"
   AddJob(output, "Running Test: " .. exe, test_exe_cmd, exe)
   SkipOutputVerification(output)
   return exe
end

settings = NewSettings()

vpath = PathDir(ModuleFilename())

-- Flatten all build outputs into the test directory
settings.cc.Output = function(settings, input)
   result = PathJoin(vpath, PathFilename(PathBase(input)) .. settings.config_ext)
   --print("Result: " .. result )
   return result
end
settings.link.Output = function(settings, input)
   result = PathJoin(vpath, PathJoin("testbin", PathFilename(PathBase(input)) .. settings.config_ext))
   --print("Result: " .. result )
   return result
end
settings.lib.Output = settings.cc.Output


if family == "windows" then
   settings.cc.defines:Add("_CRT_SECURE_NO_WARNINGS")
   settings.cc.defines:Add("strdup=_strdup")   
   settings.link.libpath:Add("test/cmocka-bin/lib/win64")
else
   settings.link.libpath:Add("test/cmocka-bin/lib/linux64")
   settings.link.flags:Add("-Wl,-rpath=test/cmocka-bin/lib/linux64")
end

settings.cc.defines:Add("UNIT_TESTING")

settings.cc.includes:Add("include")
settings.cc.includes:Add("test/cmocka-bin/include")
settings.link.libs:Add("cmocka")

ryanutilSources = Collect("src/*.c")
ryantutilObjects = Compile(settings, ryanutilSources)
ryanutilLibrary = StaticLibrary(settings, "ryanutil", ryantutilObjects)


mock_rumem_obj   = Compile(settings, "test/mock_rumem.c")
mock_ruarray_obj = Compile(settings, "test/mock_ruarray.c")
mock_math_obj = Compile(settings, "test/mock_math.c")


tests = {  
   BuildAndRunTest("test_rumem",           settings, { mock_ruarray_obj, ryanutilLibrary }, Collect("test/test_rumem.c")),
   BuildAndRunTest("test_ruarray",         settings,                   { ryanutilLibrary }, Collect("test/test_ruarray.c")),
   BuildAndRunTest("test_rulist",          settings, { mock_rumem_obj,   ryanutilLibrary }, Collect("test/test_rulist.c")),
   BuildAndRunTest("test_rulist_nomock",   settings,                   { ryanutilLibrary }, Collect("test/test_rulist_nomock.c")),
   BuildAndRunTest("test_rudense",         settings,                   { ryanutilLibrary }, Collect("test/test_rudense.c")),
   BuildAndRunTest("test_ruserialize",     settings, { mock_math_obj,    ryanutilLibrary }, Collect("test/test_ruserialize.c")),
   BuildAndRunTest("test_ruserialize_io",  settings,                   { ryanutilLibrary }, Collect("test/test_ruserialize_io.c")),
}

-- Windows needs to copy the cmocka dll file into the cwd to run the tests
if family == "windows" then
   dllcopy = CopyToDirectory("./", "test/cmocka-bin/lib/win64/cmocka.dll")
   for i, test in ipairs(tests) do
      AddDependency(test, dllcopy)
   end
end



