#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <ruarray.h>

void ruarray_swap(void * array, size_t elementSize, void * swapSpace, 
                  size_t index1, size_t index2)
{
   function_called();
   check_expected_ptr(array);
   check_expected(elementSize);
   check_expected_ptr(swapSpace);
   check_expected(index1);
   check_expected(index2);
}

size_t ruarray_copy(void * array, size_t elementSize, size_t arraySize,
                    size_t toIndex, size_t fromIndex, size_t count)
{
   function_called();
   check_expected_ptr(array);
   check_expected(elementSize);
   check_expected(arraySize);
   check_expected(toIndex);
   check_expected(fromIndex);
   check_expected(count);
   return (size_t)mock();
}
