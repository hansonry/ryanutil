#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <rumem.h>

void rumem_init(struct rumem * mem, void ** basePtr, size_t elementSize, size_t initSize)
{
   function_called();
   check_expected_ptr(mem);
   check_expected_ptr(basePtr);
   check_expected(elementSize);
   check_expected(initSize);
}

void rumem_destroy(struct rumem * mem)
{
   function_called();
   check_expected_ptr(mem);
}

void rumem_growto(struct rumem * mem, size_t targetSize)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(targetSize);
}

void rumem_shrinkto(struct rumem * mem, size_t targetSize)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(targetSize);
}

void * rumem_get(struct rumem * mem, size_t index)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(index);
   return (void *)mock();
}

enum rustatus rumem_copyin(struct rumem * mem, size_t index, const void * data)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(index);
   check_expected_ptr(data);
   return (enum rustatus)mock();
}

void rumem_copyinall(struct rumem * mem, const void * data)
{
   function_called();
   check_expected_ptr(mem);
   check_expected_ptr(data);
}

size_t rumem_copyinto(struct rumem * mem, const void * data, size_t index, size_t size)
{
   function_called();
   check_expected_ptr(mem);
   check_expected_ptr(data);
   check_expected(index);
   check_expected(size);
   return (size_t)mock();
}

enum rustatus rumem_copyout(struct rumem * mem, void * data, size_t index)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(index);
   check_expected_ptr(data);
   return (enum rustatus)mock();
}

enum rustatus rumem_swap(struct rumem * mem, size_t index1, size_t index2)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(index1);
   check_expected(index2);
   return (enum rustatus)mock();
}

size_t rumem_blockcopy(struct rumem * mem, size_t toIndex, 
                       size_t fromIndex, size_t count)
{
   function_called();
   check_expected_ptr(mem);
   check_expected(toIndex);
   check_expected(fromIndex);
   check_expected(count);
   return (size_t)mock();
}

size_t rumem_indexof(struct rumem * mem, const void * address)
{
   function_called();
   check_expected_ptr(mem);
   check_expected_ptr(address);
   return (size_t)mock();
}
