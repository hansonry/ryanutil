#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include "rumem.h"


struct thing
{
   int x;
   int y;
   int z;
};
static void util_set(struct thing * t, int x, int y, int z)
{
   t->x = x;
   t->y = y;
   t->z = z;
}

static void util_write(struct thing * thingbase, size_t count, int xoff, int yoff, int zoff)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      thingbase[i].x = xoff + i;
      thingbase[i].y = yoff + i;
      thingbase[i].z = zoff + i;
   }
}

static void util_verify(struct thing * thingbase, size_t count, int xoff, int yoff, int zoff)
{
   int i;
   // Verify everything

   for(i = 0; i < count; i++)
   {
      assert_int_equal(xoff + i, thingbase[i].x);
      assert_int_equal(yoff + i, thingbase[i].y);
      assert_int_equal(zoff + i, thingbase[i].z);
   }

}

static void test_rumem_init(void **state)
{
   struct thing * thinglist = NULL;
   struct rumem thinglistdata;

   rumem_initm(&thinglistdata, thinglist, 0);


   assert_int_equal(32, thinglistdata.size);
   assert_int_equal(sizeof(struct thing), thinglistdata.elementSize);
   assert_ptr_not_equal(NULL, thinglist);

   util_write(thinglist, thinglistdata.size, 5, 10, 15);

   util_verify(thinglist, thinglistdata.size, 5, 10, 15);


   rumem_destroy(&thinglistdata);

   assert_int_equal(0, thinglistdata.size);
   assert_int_equal(0, thinglistdata.elementSize);
   assert_ptr_equal(NULL, thinglist);
}

static void test_rumem_growto(void **state)
{
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;

   rumem_initm(&thingbasedata, thingbase, 0);
  
   rumem_growto(&thingbasedata, 64);
   assert_int_equal(64, thingbasedata.size);

   
   util_write(thingbase, thingbasedata.size, 6, 11, 16);

   util_verify(thingbase, thingbasedata.size, 6, 11, 16);


   // Grow to something smaller shouldn't change the size
   rumem_growto(&thingbasedata, 32);
   assert_int_equal(64, thingbasedata.size);

   rumem_destroy(&thingbasedata);
}

static void test_rumem_shrinkto(void **state)
{
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;

   rumem_initm(&thingbasedata, thingbase, 0);
  
   rumem_shrinkto(&thingbasedata, 16);
   assert_int_equal(16, thingbasedata.size);

   
   util_write(thingbase, thingbasedata.size, 6, 11, 16);

   util_verify(thingbase, thingbasedata.size, 6, 11, 16);

   // Shrink to something larger shouldn't change the size
   rumem_shrinkto(&thingbasedata, 32);
   assert_int_equal(16, thingbasedata.size);
   rumem_destroy(&thingbasedata);
}

static void test_rumem_get(void **state)
{
   struct thing * thingbase = NULL;
   struct thing * ptr;
   struct rumem thingbasedata;

   rumem_initm(&thingbasedata, thingbase, 0);

   ptr = rumem_get(&thingbasedata, 0);
   assert_ptr_equal(ptr, &thingbase[0]);
  
   ptr = rumem_get(&thingbasedata, thingbasedata.size - 1);
   assert_ptr_equal(ptr, &thingbase[thingbasedata.size - 1]);

   ptr = rumem_get(&thingbasedata, thingbasedata.size);
   assert_ptr_equal(ptr, NULL);

   rumem_destroy(&thingbasedata);
}



static void test_rumem_copyin(void **state)
{
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;
   struct thing localthing;

   rumem_initm(&thingbasedata, thingbase, 0);

   util_set(&thingbase[0], 0, 0, 0); 

   localthing.x = 5;
   localthing.y = 6;
   localthing.z = 7;
   assert_int_equal(rumem_copyin(&thingbasedata, 0, &localthing), e_rus_success);

   assert_int_equal(localthing.x, thingbase[0].x);
   assert_int_equal(localthing.y, thingbase[0].y);
   assert_int_equal(localthing.z, thingbase[0].z);

   // Check largest index
   util_set(&thingbase[thingbasedata.size - 1], 0, 0, 0); 

   localthing.x = -3;
   localthing.y = -6;
   localthing.z =  2;
   assert_int_equal(rumem_copyin(&thingbasedata, thingbasedata.size - 1, &localthing), e_rus_success) ;

   assert_int_equal(localthing.x, thingbase[thingbasedata.size - 1].x);
   assert_int_equal(localthing.y, thingbase[thingbasedata.size - 1].y);
   assert_int_equal(localthing.z, thingbase[thingbasedata.size - 1].z);
   
   // check overwrite does nothing
   assert_int_equal(rumem_copyin(&thingbasedata, thingbasedata.size, &localthing), e_rus_failure);
   
   // check copying in NULL
   assert_int_equal(rumem_copyin(&thingbasedata, thingbasedata.size, NULL), e_rus_failure);


   rumem_destroy(&thingbasedata);
}

static void test_rumem_copyout(void **state)
{
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;
   struct thing localthing;

   rumem_initm(&thingbasedata, thingbase, 8);

   util_set(&localthing, 0, 0, 0); 

   thingbase[0].x = 5;
   thingbase[0].y = 6;
   thingbase[0].z = 7;

   thingbase[7].x = 1;
   thingbase[7].y = 2;
   thingbase[7].z = 3;
   assert_int_equal(rumem_copyout(&thingbasedata, &localthing, 0), e_rus_success);

   assert_int_equal(localthing.x, thingbase[0].x);
   assert_int_equal(localthing.y, thingbase[0].y);
   assert_int_equal(localthing.z, thingbase[0].z);
   
   
   assert_int_equal(rumem_copyout(&thingbasedata, &localthing, 7), e_rus_success);

   assert_int_equal(localthing.x, thingbase[7].x);
   assert_int_equal(localthing.y, thingbase[7].y);
   assert_int_equal(localthing.z, thingbase[7].z);

   assert_int_equal(rumem_copyout(&thingbasedata, &localthing, 8), e_rus_failure);

   assert_int_equal(rumem_copyout(&thingbasedata, NULL, 0), e_rus_failure);

   rumem_destroy(&thingbasedata);
}

static void test_rumem_copyinall(void **state)
{
   size_t i;
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;
   struct thing localthing;

   rumem_initm(&thingbasedata, thingbase, 0);

   // zero everything
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }

   util_set(&localthing, 4, 299, -40);
   
   rumem_copyinall(&thingbasedata, &localthing);   

   
   for(i = 0; i < thingbasedata.size; i++)
   {
      assert_int_equal(localthing.x, thingbase[i].x);
      assert_int_equal(localthing.y, thingbase[i].y);
      assert_int_equal(localthing.z, thingbase[i].z);
   }

   rumem_copyinall(&thingbasedata, NULL);
   
   rumem_destroy(&thingbasedata);
}

static void test_rumem_realative_location(void **state)
{
   struct rumem thingbasedata1;
   unsigned char space1[16];
   struct thing * thingbase = NULL;
   unsigned char space2[16];
   struct rumem thingbasedata2;

   rumem_initm(&thingbasedata1, thingbase, 0);

   assert_ptr_equal(thingbase, rumem_get(&thingbasedata1, 0));
   
   util_write(thingbase, thingbasedata1.size, 99, 110, -160);

   util_verify(thingbase, thingbasedata1.size, 99, 110, -160);

   rumem_destroy(&thingbasedata1);

   rumem_initm(&thingbasedata2, thingbase, 0);

   assert_ptr_equal(thingbase, rumem_get(&thingbasedata2, 0));
   
   util_write(thingbase, thingbasedata1.size, -99, -110, 160);

   util_verify(thingbase, thingbasedata1.size, -99, -110, 160);

   rumem_destroy(&thingbasedata1);
}

struct thingpair
{
   struct thing * base;
   struct rumem basedata;
};

static void test_rumem_base_copy(void **state)
{
   struct thingpair pairs[2];
   size_t newSize;



   rumem_initm(&pairs[0].basedata, pairs[0].base, 0);

   util_write(pairs[0].base, pairs[0].basedata.size, 23, 6, 133);

   util_verify(pairs[0].base, pairs[0].basedata.size,  23, 6, 133);

   memcpy(&pairs[1], &pairs[0], sizeof(struct thingpair));

   assert_ptr_equal(pairs[1].base, rumem_get(&pairs[1].basedata, 0));
   util_write(pairs[1].base, pairs[1].basedata.size, 200, -200, 200);

   util_verify(pairs[1].base, pairs[1].basedata.size,  200, -200, 200);

   // Growby test just to be sure
   newSize = pairs[1].basedata.size + 5;
   rumem_growto(&pairs[1].basedata, newSize); 

   assert_int_equal(newSize, pairs[1].basedata.size);

   util_write(pairs[1].base, pairs[1].basedata.size, 100, -100, 100);

   util_verify(pairs[1].base, pairs[1].basedata.size,  100, -100, 100);

   rumem_destroy(&pairs[1].basedata);
}

static void test_rumem_swap(void **state)
{
   struct thing * thinglist = NULL;
   struct rumem thinglistdata;

   rumem_initm(&thinglistdata, thinglist, 8);
   assert_ptr_equal(thinglistdata.swapSpace, NULL);

   expect_function_call( ruarray_swap );   
   expect_value(         ruarray_swap, array,       thinglist);
   expect_value(         ruarray_swap, elementSize, sizeof(struct thing));
   expect_not_value(     ruarray_swap, swapSpace,   NULL);
   expect_value(         ruarray_swap, index1,      0);
   expect_value(         ruarray_swap, index2,      1);

   assert_int_equal(rumem_swap(&thinglistdata, 0, 1), e_rus_success);
   assert_ptr_not_equal(thinglistdata.swapSpace, NULL);
   
   expect_function_call( ruarray_swap );   
   expect_value(         ruarray_swap, array,       thinglist);
   expect_value(         ruarray_swap, elementSize, sizeof(struct thing));
   expect_value(         ruarray_swap, swapSpace,   thinglistdata.swapSpace);
   expect_value(         ruarray_swap, index1,      2);
   expect_value(         ruarray_swap, index2,      3);

   assert_int_equal(rumem_swap(&thinglistdata, 2, 3), e_rus_success);

   assert_int_equal(rumem_swap(&thinglistdata, 0, 0), e_rus_success);

   assert_int_equal(rumem_swap(&thinglistdata, 1, 1), e_rus_success);
   
   expect_function_call( ruarray_swap );   
   expect_value(         ruarray_swap, array,       thinglist);
   expect_value(         ruarray_swap, elementSize, sizeof(struct thing));
   expect_not_value(     ruarray_swap, swapSpace,   NULL);
   expect_value(         ruarray_swap, index1,      7);
   expect_value(         ruarray_swap, index2,      6);

   assert_int_equal(rumem_swap(&thinglistdata, 7, 6), e_rus_success);
   
   
   assert_int_equal(rumem_swap(&thinglistdata, 7, 8), e_rus_failure);
   assert_int_equal(rumem_swap(&thinglistdata, 8, 7), e_rus_failure);
   assert_int_equal(rumem_swap(&thinglistdata, 100, 200), e_rus_failure);
   assert_int_equal(rumem_swap(&thinglistdata, 100, 100), e_rus_failure);

   assert_ptr_not_equal(thinglistdata.swapSpace, NULL);
   rumem_destroy(&thinglistdata);
   assert_ptr_equal(thinglistdata.swapSpace, NULL);
}


static void test_rumem_blockcopy(void **state)
{
   struct thing * thinglist = NULL;
   struct rumem thinglistdata;

   rumem_initm(&thinglistdata, thinglist, 8);

   expect_function_call( ruarray_copy );   
   expect_value(         ruarray_copy, array,       thinglist);
   expect_value(         ruarray_copy, elementSize, sizeof(struct thing));
   expect_value(         ruarray_copy, arraySize,   thinglistdata.size);
   expect_value(         ruarray_copy, toIndex,     0);
   expect_value(         ruarray_copy, fromIndex,   1);
   expect_value(         ruarray_copy, count,       5);
   will_return(          ruarray_copy, 5);
   assert_int_equal(rumem_blockcopy(&thinglistdata, 0, 1, 5), 5);   

   expect_function_call( ruarray_copy );   
   expect_value(         ruarray_copy, array,       thinglist);
   expect_value(         ruarray_copy, elementSize, sizeof(struct thing));
   expect_value(         ruarray_copy, arraySize,   thinglistdata.size);
   expect_value(         ruarray_copy, toIndex,     1);
   expect_value(         ruarray_copy, fromIndex,   0);
   expect_value(         ruarray_copy, count,       2);
   will_return(          ruarray_copy, 2);
   assert_int_equal(rumem_blockcopy(&thinglistdata, 1, 0, 2), 2);   
   
   expect_function_call( ruarray_copy );   
   expect_value(         ruarray_copy, array,       thinglist);
   expect_value(         ruarray_copy, elementSize, sizeof(struct thing));
   expect_value(         ruarray_copy, arraySize,   thinglistdata.size);
   expect_value(         ruarray_copy, toIndex,     0);
   expect_value(         ruarray_copy, fromIndex,   7);
   expect_value(         ruarray_copy, count,       1);
   will_return(          ruarray_copy, 1);
   assert_int_equal(rumem_blockcopy(&thinglistdata, 0, 7, 1), 1);
      
   expect_function_call( ruarray_copy );   
   expect_value(         ruarray_copy, array,       thinglist);
   expect_value(         ruarray_copy, elementSize, sizeof(struct thing));
   expect_value(         ruarray_copy, arraySize,   thinglistdata.size);
   expect_value(         ruarray_copy, toIndex,     7);
   expect_value(         ruarray_copy, fromIndex,   0);
   expect_value(         ruarray_copy, count,       1);
   will_return(          ruarray_copy, 1);
   assert_int_equal(rumem_blockcopy(&thinglistdata, 7, 0, 1), 1);


   expect_function_call( ruarray_copy );   
   expect_value(         ruarray_copy, array,       thinglist);
   expect_value(         ruarray_copy, elementSize, sizeof(struct thing));
   expect_value(         ruarray_copy, arraySize,   thinglistdata.size);
   expect_value(         ruarray_copy, toIndex,     5);
   expect_value(         ruarray_copy, fromIndex,   5);
   expect_value(         ruarray_copy, count,       2);
   will_return(          ruarray_copy, 2);
   assert_int_equal(rumem_blockcopy(&thinglistdata, 5, 5, 2), 2);
   
   rumem_destroy(&thinglistdata);
}

static void test_rumem_indexof(void **state)
{
   unsigned char (* list)[8];
   struct rumem listdata;

   rumem_initm(&listdata, list, 8);

   assert_ptr_equal(rumem_get(&listdata, 0), &(list[0])[0]);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[0]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[3])[0]), 3);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[0]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[8])[0]), 8);
   assert_int_equal(rumem_indexof(&listdata, &(list[9])[0]), 8);
   assert_int_equal(rumem_indexof(&listdata, ((unsigned char*)&((list[0])[0])) - 1), 8);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[1]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[2]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[3]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[4]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[5]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[6]), 0);
   assert_int_equal(rumem_indexof(&listdata, &(list[0])[7]), 0);

   
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[1]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[2]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[3]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[4]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[5]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[6]), 7);
   assert_int_equal(rumem_indexof(&listdata, &(list[7])[7]), 7);

   assert_int_equal(rumem_indexof(&listdata, &(list[7])[8]), 8);
   
   rumem_growto(&listdata, 16);
   
   assert_int_equal(rumem_indexof(&listdata, &(list[15])[7]), 15);
   assert_int_equal(rumem_indexof(&listdata, ((unsigned char*)&((list[0])[0])) - 1), 16);
   assert_int_equal(rumem_indexof(&listdata, &(list[15])[8]), 16);
   assert_int_equal(rumem_indexof(&listdata, &(list[8])[0]), 8);
   
   rumem_destroy(&listdata);
}

static void test_rumem_size(void **state)
{
   struct thing * thinglist = NULL;
   struct rumem thinglistdata;

   rumem_initm(&thinglistdata, thinglist, 8);
   
   assert_int_equal(rumem_size(&thinglistdata), 8);
   
   rumem_growto(&thinglistdata, 16);
   
   assert_int_equal(rumem_size(&thinglistdata), 16);
   
   rumem_destroy(&thinglistdata);
}

static void util_verifycopyinto(struct rumem * thingbasedata, struct thing * thingbase, struct thing * data, size_t index, size_t size)
{
   size_t i;
   
   for(i = 0; i < index; i++)
   {
      //printf("HI0: %i\n", (int)i);
      assert_int_equal(0, thingbase[i].x);
      assert_int_equal(0, thingbase[i].y);
      assert_int_equal(0, thingbase[i].z);
   }
   
   
   for(; i < (index + size) && i < thingbasedata->size; i++)
   {
      //printf("HI1: %i\n", (int)i);
      assert_int_equal(data->x, thingbase[i].x);
      assert_int_equal(data->y, thingbase[i].y);
      assert_int_equal(data->z, thingbase[i].z);
   }
   
   for(; i < thingbasedata->size; i++)
   {
      //printf("HI2: %i\n", (int)i);
      assert_int_equal(0, thingbase[i].x);
      assert_int_equal(0, thingbase[i].y);
      assert_int_equal(0, thingbase[i].z);
   }

}

static void test_rumem_copyinto(void **state)
{
   size_t i;
   struct thing * thingbase = NULL;
   struct rumem thingbasedata;
   struct thing localthing;

   rumem_initm(&thingbasedata, thingbase, 16);

   util_set(&localthing, 4, 299, -40);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 0, 16), 16);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 0, 16);

   util_set(&localthing, 55, 66, 77);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 1, 14), 14);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 1, 14);

   util_set(&localthing, -55, -66, -77);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 6, 14), 10);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 6, 14);

   util_set(&localthing, 123, 456, 789);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 16, 14), 0);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 16, 14);

   util_set(&localthing, 123, 456, 789);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 15, 14), 1);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 15, 14);

   util_set(&localthing, 123, 456, 789);
   for(i = 0; i < thingbasedata.size; i++)
   {
      util_set(&thingbase[i], 0, 0, 0);
   }
   assert_int_equal(rumem_copyinto(&thingbasedata, &localthing, 0, 0), 0);
   util_verifycopyinto(&thingbasedata, thingbase,  &localthing, 0, 0);

   assert_int_equal(rumem_copyinto(&thingbasedata, NULL, 1, 4), 0);

   rumem_destroy(&thingbasedata);
}


int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_rumem_init),
      cmocka_unit_test(test_rumem_growto),
      cmocka_unit_test(test_rumem_shrinkto),
      cmocka_unit_test(test_rumem_get),
      cmocka_unit_test(test_rumem_copyin),
      cmocka_unit_test(test_rumem_copyinall),
      cmocka_unit_test(test_rumem_copyout),
      cmocka_unit_test(test_rumem_realative_location),
      cmocka_unit_test(test_rumem_base_copy),
      cmocka_unit_test(test_rumem_swap),
      cmocka_unit_test(test_rumem_blockcopy),
      cmocka_unit_test(test_rumem_indexof),
      cmocka_unit_test(test_rumem_size),
      cmocka_unit_test(test_rumem_copyinto),
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


