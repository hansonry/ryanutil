#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <stdbool.h>
#include "rudense.h"


struct thing
{
   int x;
   int y;
   int z;
};
static void util_set(struct thing * t, int x, int y, int z)
{
   t->x = x;
   t->y = y;
   t->z = z;
}

#define COMP_INT   0
#define COMP_THING 1
#define COMP_FLOAT 2
#define COMP__SIZE 3

static size_t comps[3] =
{
   sizeof(int),
   sizeof(struct thing),
   sizeof(float)
};

struct allcompsptr
{
   int * health;
   struct thing * position;
   float * speed;
};


static void util_setallto(size_t * array, size_t size, size_t value)
{
   size_t i;
   for(i = 0; i < size; i++)
   {
      array[i] = value;
   }
}

static void util_setramp(size_t * array, size_t size, size_t start, int offset)
{
   size_t i;
   size_t value;

   value = start;
   for(i = 0; i < size; i++)
   {
      array[i] = value;
      value += offset;
   }
}

static bool util_isjump(struct rudense * dense, 
                        size_t address, 
                        size_t * target_address)
{
   unsigned char * bitfield;
   unsigned char mask;
   size_t i;

   bitfield = &dense->base[address];
   mask = 1;
   for(i = 0; i < dense->componentTableSize; i++)
   {
      if((i % 8) == 0 && i != 0)
      {
         mask = 1;
         bitfield++;
      }
      if((*bitfield & mask) == mask)
      {
         return false;
      }
      mask = mask << 1;
   }

   if(target_address != NULL)
   {
      *target_address = *((size_t*)&dense->base[address + dense->bitFieldSize]);
   }
   return true;
}


static void test_rudense_init(void **state)
{
   struct rudense dense;
   size_t bitFieldSize;
   size_t comps2[2] = 
   {
      sizeof(int),
      sizeof(float)
   };
   
   dense.base = NULL;
   dense.size = 0;
   dense.growBy = 0;
   dense.count = 1;
   rudense_init(&dense, comps, COMP__SIZE, 0, 0);
   
   assert_int_equal(dense.size,               0x500);
   assert_int_equal(dense.growBy,             0x100);
   assert_ptr_not_equal(dense.base,           NULL);
   assert_int_equal(dense.count,              0);
   assert_ptr_equal(dense.componentTable,     comps);
   assert_int_equal(dense.componentTableSize, COMP__SIZE);
   bitFieldSize = dense.bitFieldSize;
   
   
   dense.count = 1;
   rudense_destroy(&dense);
   assert_int_equal(dense.size,               0);
   assert_int_equal(dense.growBy,             0);
   assert_ptr_equal(dense.base,               NULL);
   assert_int_equal(dense.count,              0);
   assert_ptr_equal(dense.componentTable,     NULL);
   assert_int_equal(dense.componentTableSize, 0);

   dense.base = NULL;
   dense.size = 0;
   dense.count = 1;
   rudense_init(&dense, comps2, 2, 2000, 20);
   
   assert_int_equal(dense.size,               2000);
   assert_int_equal(dense.growBy,             20);
   assert_ptr_not_equal(dense.base,           NULL);
   assert_ptr_not_equal(dense.base,           NULL);
   assert_int_equal(dense.count,              0);
   assert_ptr_equal(dense.componentTable,     comps2);
   assert_int_equal(dense.componentTableSize, 2);
   
   dense.count = 1;
   rudense_destroy(&dense);
   assert_int_equal(dense.size,               0);
   assert_int_equal(dense.growBy,             0);
   assert_ptr_equal(dense.base,               NULL);
   assert_int_equal(dense.count,              0);
   assert_ptr_equal(dense.componentTable,     NULL);
   assert_int_equal(dense.componentTableSize, 0);


   // Check for minimum initial size
   rudense_init(&dense, comps2, 2, bitFieldSize + sizeof(size_t) - 1, 0);
   assert_int_equal(dense.bitFieldSize, bitFieldSize);
   assert_int_equal(dense.size, bitFieldSize + sizeof(size_t));
   rudense_destroy(&dense);

   rudense_init(&dense, comps2, 2, bitFieldSize + sizeof(size_t), 0);
   assert_int_equal(dense.bitFieldSize, bitFieldSize);
   assert_int_equal(dense.size, bitFieldSize + sizeof(size_t));
   rudense_destroy(&dense);

   rudense_init(&dense, comps2, 2, 1, 0);
   assert_int_equal(dense.bitFieldSize, bitFieldSize);
   assert_int_equal(dense.size, bitFieldSize + sizeof(size_t));
   rudense_destroy(&dense);
}

static void test_rudense_init_bitfield(void **state)
{
   struct rudense dense;
   size_t bigCompSize[128];

   util_setallto(bigCompSize, 128, sizeof(int));

   rudense_init(&dense, comps, COMP__SIZE, 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int));

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);

   rudense_init(&dense, bigCompSize, (sizeof(int) * 8) - 1, 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int));

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);

   rudense_init(&dense, bigCompSize, (sizeof(int) * 8), 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int));

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);

   rudense_init(&dense, bigCompSize, (sizeof(int) * 8) + 1, 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int) * 2);

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);

   rudense_init(&dense, bigCompSize, (sizeof(int) * 16), 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int) * 2);

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);

   rudense_init(&dense, bigCompSize, (sizeof(int) * 16) + 1, 0, 0);
   assert_int_equal(dense.bitFieldSize, sizeof(int) * 3);

   rudense_destroy(&dense);
   assert_int_equal(dense.bitFieldSize, 0);
}

static size_t computeSize(struct rudense * dense, 
                          size_t * elems, size_t elemSize)
{
   size_t size;
   size_t i;
   size = dense->bitFieldSize;
   for(i = 0; i < elemSize; i++)
   {
      size += dense->componentTable[elems[i]];
   }
   
   if(size < dense->bitFieldSize + sizeof(size_t))
   {
      size = dense->bitFieldSize + sizeof(size_t);
   }
   return size; 
}

static void test_rudense_add(void **state)
{
   struct rudense dense;
   size_t address;
   size_t expected_address;
   size_t elems[2] = {
      COMP_THING,
      COMP_INT,
   };
   size_t largeComp[32];
   size_t largeElems[32];

   util_setallto(largeComp, 32, sizeof(int));
   util_setramp(largeElems, 32, 0, 1);

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);

   expected_address = 0;
   address = rudense_add(&dense, elems, 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 1);
   assert_int_equal(dense.base[address], 0x03);

   expected_address += computeSize(&dense, elems, 2);
   address = rudense_add(&dense, elems, 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address], 0x03);

   expected_address += computeSize(&dense, elems, 2);
   address = rudense_add(&dense, elems, 1);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address], 0x02);
   
   expected_address += computeSize(&dense, elems, 1);
   address = rudense_add(&dense, &elems[1], 1);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 4);
   assert_int_equal(dense.base[address], 0x01);

   expected_address += computeSize(&dense, &elems[1], 1);
   address = rudense_add(&dense, &elems[1], 1);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 5);
   assert_int_equal(dense.base[address], 0x01);


   rudense_destroy(&dense);

   rudense_init(&dense, largeComp, 17, 0x400, 0);

   expected_address = 0;
   address = rudense_add(&dense, &largeElems[0], 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 1);
   assert_int_equal(dense.base[address],     0x03);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x00);

   expected_address += computeSize(&dense, &largeElems[0], 2);
   address = rudense_add(&dense, &largeElems[2], 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address],     0x0C);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x00);
   
   expected_address += computeSize(&dense, &largeElems[2], 2);
   address = rudense_add(&dense, &largeElems[4], 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address],     0x30);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x00);
   
   expected_address += computeSize(&dense, &largeElems[4], 2);
   address = rudense_add(&dense, &largeElems[6], 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 4);
   assert_int_equal(dense.base[address],     0xC0);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x00);
   
   expected_address += computeSize(&dense, &largeElems[6], 2);
   address = rudense_add(&dense, &largeElems[0], 8);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 5);
   assert_int_equal(dense.base[address],     0xFF);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x00);
   
   expected_address += computeSize(&dense, &largeElems[0], 8);
   address = rudense_add(&dense, &largeElems[8], 8);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 6);
   assert_int_equal(dense.base[address],     0x00);
   assert_int_equal(dense.base[address + 1], 0xFF);
   assert_int_equal(dense.base[address + 2], 0x00);
   
   expected_address += computeSize(&dense, &largeElems[8], 8);
   address = rudense_add(&dense, &largeElems[15], 2);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 7);
   assert_int_equal(dense.base[address],     0x00);
   assert_int_equal(dense.base[address + 1], 0x80);
   assert_int_equal(dense.base[address + 2], 0x01);

   expected_address += computeSize(&dense, &largeElems[15], 2);
   address = rudense_add(&dense, &largeElems[15], 3);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 8);
   assert_int_equal(dense.base[address],     0x00);
   assert_int_equal(dense.base[address + 1], 0x80);
   assert_int_equal(dense.base[address + 2], 0x01);
   
   expected_address += computeSize(&dense, &largeElems[15], 2);
   address = rudense_add(&dense, &largeElems[16], 1);
   assert_int_equal(address, expected_address);
   assert_int_equal(dense.count, 9);
   assert_int_equal(dense.base[address],     0x00);
   assert_int_equal(dense.base[address + 1], 0x00);
   assert_int_equal(dense.base[address + 2], 0x01);
   
   rudense_destroy(&dense);
}

static void test_rudense_add_remove(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t last_address[16];
   size_t * jumpAddress;
   unsigned char * bitfield;
   size_t elems[2] = {
      COMP_THING,
      COMP_INT,
   };

   size_t tinyComps[32];
   size_t tinyElems[32];

   util_setallto(tinyComps, 32, 1);
   util_setramp(tinyElems, 32, 0, 1);

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);
   
   // Add Two, remove the first, re-add the first all the same size
   address[0] = rudense_add(&dense, elems, 2);   
   address[1] = rudense_add(&dense, elems, 2);   
   rudense_remove(&dense, address[0]);
   assert_int_equal(address[0], 0);
   assert_int_equal(dense.count, 1);

   address[0] = rudense_add(&dense, elems, 2);   
   assert_int_equal(address[0], 0);
   assert_int_equal(dense.count, 2);

   // Add one more and remove the root node again and re-add it 
   address[2] = rudense_add(&dense, elems, 2);   
   
   rudense_remove(&dense, address[0]);

   address[0] = rudense_add(&dense, elems, 2);   
   assert_int_equal(address[0], 0);
   assert_int_equal(dense.count, 3);

   // Remove the 2nd one and re-add
   last_address[1] = address[1];
   rudense_remove(&dense, address[1]);

   address[1] = rudense_add(&dense, elems, 2);   
   assert_int_equal(address[1], last_address[1]);
   assert_int_equal(dense.count, 3);
   
   rudense_destroy(&dense);
   
   rudense_init(&dense, tinyComps, 32, 0x400, 0);

   // Add 3 very large
   address[0] = rudense_add(&dense, tinyElems, 31);   
   address[1] = rudense_add(&dense, tinyElems, 31);   
   address[2] = rudense_add(&dense, tinyElems, 31);   
  
   // Remove the first and attempt to add something larger
   rudense_remove(&dense, address[0]);
   address[3] = rudense_add(&dense, tinyElems, 32);
   assert_int_not_equal(address[3], address[0]);  
   assert_int_equal(dense.count, 3);
   
   // Attempt to add something smaller but not small enough to fit the expansion
   address[4] = rudense_add(&dense, tinyElems, 30);
   assert_int_not_equal(address[4], address[0]);  
   assert_int_equal(dense.count, 4);

   // Attempt to add something smaller but not small enough to fit the expansion
   address[5] = rudense_add(&dense, tinyElems, 31 - (dense.bitFieldSize + sizeof(size_t) - 1));
   assert_int_not_equal(address[5], address[0]);  
   assert_int_equal(dense.count, 5);
    
   // Attempt to add something just small enough to fit
   address[0] = rudense_add(&dense, tinyElems, 31 - (dense.bitFieldSize + sizeof(size_t)));
   jumpAddress = (size_t *)&dense.base[address[1] - sizeof(size_t)];
   bitfield = &dense.base[address[1] - sizeof(size_t) - dense.bitFieldSize];
   assert_int_equal(address[0], 0);  
   assert_int_equal(*jumpAddress, address[1]);   
   assert_int_equal(bitfield[0], 0);
   assert_int_equal(bitfield[1], 0);
   assert_int_equal(bitfield[2], 0);
   assert_int_equal(bitfield[3], 0);
   assert_int_equal(dense.count, 6);
 
 
   
 
   // Remove the first and patch it back in with the original size
   rudense_remove(&dense, address[0]);
   rudense_mergeunused(&dense);
   address[0] = rudense_add(&dense, tinyElems, 31);   
   assert_int_equal(address[0], 0);  
   assert_int_equal(dense.count, 6);

   // Remove the second and attempt to add something larger
   rudense_remove(&dense, address[1]);
   address[6] = rudense_add(&dense, tinyElems, 32);
   assert_int_not_equal(address[6], address[1]);  
   assert_int_equal(dense.count, 6);
   
   // Attempt to add something smaller but not small enough to fit the expansion
   address[7] = rudense_add(&dense, tinyElems, 30);
   assert_int_not_equal(address[7], address[1]);  
   assert_int_equal(dense.count, 7);

   // Attempt to add something smaller but not small enough to fit the expansion
   address[8] = rudense_add(&dense, tinyElems, 31 - (dense.bitFieldSize + sizeof(size_t) - 1));
   assert_int_not_equal(address[8], address[1]);  
   assert_int_equal(dense.count, 8);
    
   // Attempt to add something just small enough to fit
   last_address[1] = address[1];
   address[1] = rudense_add(&dense, tinyElems, 31 - (dense.bitFieldSize + sizeof(size_t)));
   jumpAddress = (size_t *)&dense.base[address[2] - sizeof(size_t)];
   bitfield = &dense.base[address[2] - sizeof(size_t) - dense.bitFieldSize];
   assert_int_equal(address[1], last_address[1]);  
   assert_int_equal(*jumpAddress, address[2]);
   assert_int_equal(bitfield[0], 0);
   assert_int_equal(bitfield[1], 0);
   assert_int_equal(bitfield[2], 0);
   assert_int_equal(bitfield[3], 0);
   assert_int_equal(dense.count, 9);

   // Remove address 1 and address 3 and attempt to add them back in the 
   // right spots
   last_address[1] = address[1];
   last_address[3] = address[3];
   
   rudense_remove(&dense, address[1]); 
   rudense_remove(&dense, address[3]); 
   assert_int_equal(dense.count, 7);
   
   rudense_mergeunused(&dense);

   address[1] = rudense_add(&dense, tinyElems, 31);
   assert_int_equal(dense.count, 8);
   assert_int_equal(address[1], last_address[1]);  

   // Note: Added a 32 byte sized element in this slot before
   address[3] = rudense_add(&dense, tinyElems, 32);
   assert_int_equal(dense.count, 9);
   assert_int_equal(address[3], last_address[3]);  
 
   rudense_destroy(&dense);
}

static void test_rudense_remove(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t * jumpAddress;
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);

   // Remove empty
   assert_int_equal(rudense_remove(&dense, 0), e_rus_failure);
   assert_int_equal(dense.count, 0);
   
   // Add and remove
   address[0] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[0]), e_rus_success);
   assert_int_equal(dense.count, 0);

   // Repeat to ensure consistency
   assert_int_equal(rudense_remove(&dense, 0), e_rus_failure);
   assert_int_equal(dense.count, 0);
   
   address[0] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[0]), e_rus_success);
   assert_int_equal(dense.count, 0);

   // Fail to find address
   assert_int_equal(rudense_remove(&dense, 0), e_rus_failure);
   assert_int_equal(dense.count, 0);
   
   address[0] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[0] + 1), e_rus_failure);
   assert_int_equal(dense.count, 1);

   // Add another one and remove the first;
   address[1] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[0]), e_rus_success);
   assert_int_equal(dense.count, 1);
  
   // Add two more and remove the middle
   address[0] = rudense_add(&dense, elems, 2);   
   address[2] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address[1]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[1] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[2]);
 
   // Add back the the one in the middle and remove the one on the end
   address[1] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address[2]], 0x00);

   // Add back the the one at the end and remove it again
   address[2] = rudense_add(&dense, elems, 2);   
   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address[2]], 0x00);
   
   // Attempt to remove the same element
   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_failure);
   
   // Remove the now current last element
   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.count, 1);
   assert_int_equal(dense.base[address[1]], 0x00);
   
   // Add back three elements and attempt to remove two middle elements
   address[1] = rudense_add(&dense, elems, 2);
   address[2] = rudense_add(&dense, elems, 2);   
   address[3] = rudense_add(&dense, elems, 2);   
   
   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address[1]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[1] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[2]);


   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address[1]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[1] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[2]);
   jumpAddress = (size_t*)&dense.base[address[2] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[3]);

   // Add back the two and remove them in reverse order
   address[1] = rudense_add(&dense, elems, 2);
   address[2] = rudense_add(&dense, elems, 2);   

   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address[2]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[2] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[3]);

   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.count, 2);
   assert_int_equal(dense.base[address[1]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[2] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[3]);
   jumpAddress = (size_t*)&dense.base[address[1] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[2]);


   // Add back the two and remove the index 1 then index 0
   address[1] = rudense_add(&dense, elems, 2);
   address[2] = rudense_add(&dense, elems, 2);   

   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address[1]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[1] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[2]);
   
   assert_int_equal(rudense_remove(&dense, address[0]), e_rus_success);
   assert_int_equal(dense.count, 2);
   
   // Add back the two and remove the index 2 then index 3
   address[0] = rudense_add(&dense, elems, 2);
   address[1] = rudense_add(&dense, elems, 2);   
   
   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.count, 3);
   assert_int_equal(dense.base[address[2]], 0x00);
   jumpAddress = (size_t*)&dense.base[address[2] + dense.bitFieldSize];
   assert_int_equal(*jumpAddress, address[3]);
   
   assert_int_equal(rudense_remove(&dense, address[3]), e_rus_success);
   assert_int_equal(dense.count, 2);
    
   rudense_destroy(&dense);
   
   
   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);
   
   // Add three and attempt to remove the first one
   address[0] = rudense_add(&dense, elems, 3);
   address[1] = rudense_add(&dense, elems, 2);
   address[2] = rudense_add(&dense, elems, 1);
   assert_int_equal(rudense_remove(&dense, address[1]), e_rus_success);
   assert_int_equal(dense.base[address[1]], 0);
   assert_int_equal(rudense_remove(&dense, address[2]), e_rus_success);
   assert_int_equal(dense.base[address[2]], 0);
   assert_int_equal(rudense_remove(&dense, address[0]), e_rus_success);
   
   rudense_destroy(&dense);
}




static void test_rudense_get(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   size_t elems_nothing[2] = {
      COMP_INT,
      COMP_FLOAT
   };


   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);
   
   // Add one with all three elements
   address[0] = rudense_add(&dense, elems, 3);
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize], 
                    rudense_get(&dense, address[0], COMP_INT));
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize + 
                                comps[COMP_INT]], 
                    rudense_get(&dense, address[0], COMP_THING));
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize + 
                                comps[COMP_INT] + comps[COMP_THING]], 
                    rudense_get(&dense, address[0], COMP_FLOAT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[0], 3));
   assert_ptr_equal(NULL, rudense_get(&dense, address[0], 4));

   // Add one with just the first two elements
   address[1] = rudense_add(&dense, elems, 2);
   assert_ptr_equal(&dense.base[address[1] + dense.bitFieldSize], 
                    rudense_get(&dense, address[1], COMP_INT));
   assert_ptr_equal(&dense.base[address[1] + dense.bitFieldSize + 
                                comps[COMP_INT]], 
                    rudense_get(&dense, address[1], COMP_THING));
   assert_ptr_equal(NULL, rudense_get(&dense, address[1], COMP_FLOAT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[1], 3));
   assert_ptr_equal(NULL, rudense_get(&dense, address[1], 4));
   
   // Test the first address to make sure those are all still good
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize], 
                    rudense_get(&dense, address[0], COMP_INT));
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize + 
                                comps[COMP_INT]], 
                    rudense_get(&dense, address[0], COMP_THING));
   assert_ptr_equal(&dense.base[address[0] + dense.bitFieldSize + 
                                comps[COMP_INT] + comps[COMP_THING]], 
                    rudense_get(&dense, address[0], COMP_FLOAT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[0], 3));
   assert_ptr_equal(NULL, rudense_get(&dense, address[0], 4));


   // Add one with just the last two elements
   address[2] = rudense_add(&dense, &elems[1], 2);
   assert_ptr_equal(NULL, rudense_get(&dense, address[2], COMP_INT));
   assert_ptr_equal(&dense.base[address[2] + dense.bitFieldSize], 
                    rudense_get(&dense, address[2], COMP_THING));
   assert_ptr_equal(&dense.base[address[2] + dense.bitFieldSize + 
                                comps[COMP_THING]], 
                    rudense_get(&dense, address[2], COMP_FLOAT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[2], 3));
   assert_ptr_equal(NULL, rudense_get(&dense, address[2], 4));
   
   // Add one with just the two outsize elements
   address[3] = rudense_add(&dense, elems_nothing, 2);
   assert_ptr_equal(&dense.base[address[3] + dense.bitFieldSize], 
                    rudense_get(&dense, address[3], COMP_INT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[3], COMP_THING));
   assert_ptr_equal(&dense.base[address[3] + dense.bitFieldSize + 
                                comps[COMP_INT]], 
                    rudense_get(&dense, address[3], COMP_FLOAT));
   assert_ptr_equal(NULL, rudense_get(&dense, address[3], 3));
   assert_ptr_equal(NULL, rudense_get(&dense, address[3], 4));
   
   rudense_destroy(&dense);
}

static void test_rudense_getall(void **state)
{
   struct rudense dense;
   size_t address[16];
   struct allcompsptr all_struct;
   void * all_array[4];
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   size_t elems_nothing[2] = {
      COMP_INT,
      COMP_FLOAT
   };

   // Note: This test relies on rudense_get being correct
   

   
   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);

   // Add one with all three elements   
   all_struct.health   = (void*)1;
   all_struct.position = (void*)1;
   all_struct.speed    = (void*)1;
   
   all_array[COMP_INT]   = (void*)1;
   all_array[COMP_THING] = (void*)1;
   all_array[COMP_FLOAT] = (void*)1;
   all_array[3]          = (void*)1;

   address[0] = rudense_add(&dense, elems, 3);
   assert_ptr_equal(rudense_getall(&dense, address[0], &all_struct), &all_struct);
   assert_ptr_equal(rudense_getall(&dense, address[0], &all_array), &all_array);
   assert_ptr_equal(all_struct.health,   rudense_get(&dense, address[0], COMP_INT));
   assert_ptr_equal(all_struct.position, rudense_get(&dense, address[0], COMP_THING));
   assert_ptr_equal(all_struct.speed,    rudense_get(&dense, address[0], COMP_FLOAT));
   
   
   assert_ptr_equal(all_array[COMP_INT],   rudense_get(&dense, address[0], COMP_INT));
   assert_ptr_equal(all_array[COMP_THING], rudense_get(&dense, address[0], COMP_THING));
   assert_ptr_equal(all_array[COMP_FLOAT], rudense_get(&dense, address[0], COMP_FLOAT));
   assert_ptr_equal(all_array[3],          (void*)1);

   // Add one with just the first two elements

   all_struct.health   = (void*)1;
   all_struct.position = (void*)1;
   all_struct.speed    = (void*)1;
   
   all_array[COMP_INT]   = (void*)1;
   all_array[COMP_THING] = (void*)1;
   all_array[COMP_FLOAT] = (void*)1;
   all_array[3]          = (void*)1;
   
   address[1] = rudense_add(&dense, elems, 2);
   assert_ptr_equal(rudense_getall(&dense, address[1], &all_struct), &all_struct);
   assert_ptr_equal(rudense_getall(&dense, address[1], &all_array), &all_array);
   assert_ptr_equal(all_struct.health,   rudense_get(&dense, address[1], COMP_INT));
   assert_ptr_equal(all_struct.position, rudense_get(&dense, address[1], COMP_THING));
   assert_ptr_equal(all_struct.speed,    rudense_get(&dense, address[1], COMP_FLOAT));
   
   
   assert_ptr_equal(all_array[COMP_INT],   rudense_get(&dense, address[1], COMP_INT));
   assert_ptr_equal(all_array[COMP_THING], rudense_get(&dense, address[1], COMP_THING));
   assert_ptr_equal(all_array[COMP_FLOAT], rudense_get(&dense, address[1], COMP_FLOAT));
   assert_ptr_equal(all_array[3],          (void*)1);


   // Add one with just the last two elements
   all_struct.health   = (void*)1;
   all_struct.position = (void*)1;
   all_struct.speed    = (void*)1;
   
   all_array[COMP_INT]   = (void*)1;
   all_array[COMP_THING] = (void*)1;
   all_array[COMP_FLOAT] = (void*)1;
   all_array[3]          = (void*)1;
   address[2] = rudense_add(&dense, &elems[1], 2);
   assert_ptr_equal(rudense_getall(&dense, address[2], &all_struct), &all_struct);
   assert_ptr_equal(rudense_getall(&dense, address[2], &all_array), &all_array);
   assert_ptr_equal(all_struct.health,   rudense_get(&dense, address[2], COMP_INT));
   assert_ptr_equal(all_struct.position, rudense_get(&dense, address[2], COMP_THING));
   assert_ptr_equal(all_struct.speed,    rudense_get(&dense, address[2], COMP_FLOAT));
   
   
   assert_ptr_equal(all_array[COMP_INT],   rudense_get(&dense, address[2], COMP_INT));
   assert_ptr_equal(all_array[COMP_THING], rudense_get(&dense, address[2], COMP_THING));
   assert_ptr_equal(all_array[COMP_FLOAT], rudense_get(&dense, address[2], COMP_FLOAT));
   assert_ptr_equal(all_array[3],          (void*)1);


   // Add one with just the two outsize elements
   all_struct.health   = (void*)1;
   all_struct.position = (void*)1;
   all_struct.speed    = (void*)1;
   
   all_array[COMP_INT]   = (void*)1;
   all_array[COMP_THING] = (void*)1;
   all_array[COMP_FLOAT] = (void*)1;
   all_array[3]          = (void*)1;

   address[3] = rudense_add(&dense, elems_nothing, 2);
   
   assert_ptr_equal(rudense_getall(&dense, address[3], &all_struct), &all_struct);
   assert_ptr_equal(rudense_getall(&dense, address[3], &all_array), &all_array);
   assert_ptr_equal(all_struct.health,   rudense_get(&dense, address[3], COMP_INT));
   assert_ptr_equal(all_struct.position, rudense_get(&dense, address[3], COMP_THING));
   assert_ptr_equal(all_struct.speed,    rudense_get(&dense, address[3], COMP_FLOAT));
   
   
   assert_ptr_equal(all_array[COMP_INT],   rudense_get(&dense, address[3], COMP_INT));
   assert_ptr_equal(all_array[COMP_THING], rudense_get(&dense, address[3], COMP_THING));
   assert_ptr_equal(all_array[COMP_FLOAT], rudense_get(&dense, address[3], COMP_FLOAT));
   assert_ptr_equal(all_array[3],          (void*)1);



   rudense_destroy(&dense);
}


static void test_rudense_isaddress(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);
   
   assert_int_equal(rudense_isaddress(&dense, 0), false);
   
   address[0] = rudense_add(&dense, elems, 3);
   address[1] = rudense_add(&dense, elems, 2);
   address[2] = rudense_add(&dense, elems, 1);
   assert_int_equal(rudense_isaddress(&dense, address[0]), true);
   assert_int_equal(rudense_isaddress(&dense, address[0] + 1), false);
   assert_int_equal(rudense_isaddress(&dense, address[1] - 1), false);
   assert_int_equal(rudense_isaddress(&dense, address[1]), true);
   assert_int_equal(rudense_isaddress(&dense, address[1] + 1), false);
   assert_int_equal(rudense_isaddress(&dense, address[2] - 1), false);
   assert_int_equal(rudense_isaddress(&dense, address[2]), true);
   assert_int_equal(rudense_isaddress(&dense, address[2] + 1), false);
   
   rudense_destroy(&dense);
}

static void test_rudense_iterator(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t expected_address[16];
   size_t iter_address;
   size_t count;
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);



   // Test empty set
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      count ++;
   }
   assert_int_equal(count, 0);

   // Test one item
   address[0] = rudense_add(&dense, elems, 3);
   expected_address[0] = address[0];
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 1);
   
   
   // Test two items
   address[1] = rudense_add(&dense, elems, 2);
   expected_address[1] = address[1];
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 2);

   // Test three items
   address[2] = rudense_add(&dense, elems, 1);
   expected_address[2] = address[2];
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 3);

   // Remove the middle one and test again
   (void)rudense_remove(&dense, address[1]);
   expected_address[1] = address[2];
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 2);
   
   // Remove the one on the end and test again
   (void)rudense_remove(&dense, address[2]);
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 1);

   // Remove the last one
   (void)rudense_remove(&dense, address[0]);
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 0);

   // Add 3 more and test again
   address[0] = rudense_add(&dense, elems, 3);
   address[1] = rudense_add(&dense, elems, 3);
   address[2] = rudense_add(&dense, elems, 3);
   expected_address[0] = address[0];
   expected_address[1] = address[1];
   expected_address[2] = address[2];
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 3);

   // Remove the one on the end 
   (void)rudense_remove(&dense, address[2]);
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      count ++;
   }
   assert_int_equal(count, 2);
   
   
   // Add two more and test remove in the middle of the loop
   address[2] = rudense_add(&dense, elems, 3);
   address[3] = rudense_add(&dense, elems, 3);
   
   expected_address[0] = address[0];
   expected_address[1] = address[1];
   expected_address[2] = address[3];
   
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      if(count == 1)
      {
         (void)rudense_remove(&dense, address[1]);
         (void)rudense_remove(&dense, address[2]);
      }
      
      count ++;
   }
   assert_int_equal(count, 2);
   
   // Loop and add back two at the first address.

   expected_address[0] = address[0];
   expected_address[1] = address[1];
   expected_address[2] = address[2];
   expected_address[3] = address[3];
   
   count = 0;
   iter_address = rudense_iterstart(&dense);
   while(rudense_iternext(&dense, &iter_address))
   {
      assert_int_equal(iter_address, expected_address[count]);
      if(count == 0)
      {
         address[1] = rudense_add(&dense, elems, 3);
         address[2] = rudense_add(&dense, elems, 3);
      }      
      count ++;
   }
   assert_int_equal(count, 4);

   
   rudense_destroy(&dense);
}

static void test_rudense_mergeunused(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t target_address;
   size_t elems[3] = {
      COMP_INT,
      COMP_THING,
      COMP_FLOAT
   };

   rudense_init(&dense, comps, COMP__SIZE, 0x400, 0);


   // test empty merge
   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, 0, &target_address), true);
   assert_int_equal(target_address, 0);

   // Add One and remove it
   address[0] = rudense_add(&dense, elems, 3);
   rudense_remove(&dense, address[0]);
   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, 0, &target_address), true);
   assert_int_equal(target_address, 0);
  
   // Add 5 and remove everything but the middle one
   address[0] = rudense_add(&dense, elems, 3);
   address[1] = rudense_add(&dense, elems, 3);
   address[2] = rudense_add(&dense, elems, 3);
   address[3] = rudense_add(&dense, elems, 3);
   address[4] = rudense_add(&dense, elems, 3);

   rudense_remove(&dense, address[0]);
   rudense_remove(&dense, address[1]);

   rudense_remove(&dense, address[3]);
   rudense_remove(&dense, address[4]);

   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, address[0], &target_address), true);
   assert_int_equal(target_address, address[2]);
   assert_int_equal(util_isjump(&dense, address[2], &target_address), false);
   assert_int_equal(util_isjump(&dense, address[3], &target_address), true);
   assert_int_equal(target_address, address[3]);


   // Add back everything and Remove 1,2, and 4
   address[0] = rudense_add(&dense, elems, 3);
   address[1] = rudense_add(&dense, elems, 3);
   
   address[3] = rudense_add(&dense, elems, 3);
   address[4] = rudense_add(&dense, elems, 3);

   rudense_remove(&dense, address[1]);
   rudense_remove(&dense, address[2]);

   rudense_remove(&dense, address[4]);

   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, address[0], &target_address), false);
   assert_int_equal(util_isjump(&dense, address[1], &target_address), true);
   assert_int_equal(target_address, address[3]);
   assert_int_equal(util_isjump(&dense, address[3], &target_address), false);
   assert_int_equal(util_isjump(&dense, address[4], &target_address), true);
   assert_int_equal(target_address, address[4]);
   
   // Add back what was missing and remove 0, 2 and 3

   address[1] = rudense_add(&dense, elems, 3);
   address[2] = rudense_add(&dense, elems, 3);
   address[4] = rudense_add(&dense, elems, 3);
   
   rudense_remove(&dense, address[0]);
   rudense_remove(&dense, address[2]);
   rudense_remove(&dense, address[3]);
  
   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, address[0], &target_address), true);
   assert_int_equal(target_address, address[1]);
   assert_int_equal(util_isjump(&dense, address[1], &target_address), false);
   assert_int_equal(util_isjump(&dense, address[2], &target_address), true);
   assert_int_equal(target_address, address[4]);
   assert_int_equal(util_isjump(&dense, address[4], &target_address), false);

   // Remove the reset 
   rudense_remove(&dense, address[1]);
   rudense_remove(&dense, address[4]);

   rudense_mergeunused(&dense);
   assert_int_equal(util_isjump(&dense, address[0], &target_address), true);
   assert_int_equal(target_address, address[0]);

   rudense_destroy(&dense);
}

static void test_rudense_grow(void **state)
{
   struct rudense dense;
   size_t address[16];
   size_t cmpSize;
   size_t initSize;
   size_t growBy;
   size_t bitFieldSize;
   size_t i;
   size_t * jumpAddress;
   size_t elems[3] = {
      COMP_THING,
      COMP_INT,
      COMP_FLOAT
   };
   
   growBy = 0x20;
   rudense_init(&dense, comps, COMP__SIZE, 1, growBy);
   initSize = dense.size; 
   bitFieldSize = dense.bitFieldSize;
   cmpSize = computeSize(&dense, &elems[0], 2);

   assert_true(cmpSize <= growBy);

   for(i = 0; i < 50; i++)
   {
      (void)rudense_add(&dense, &elems[0], 2);
      assert_int_equal((dense.size - initSize) % growBy, 0);
      assert_int_equal((dense.size - initSize) / growBy, 
                       (dense.count * cmpSize + (growBy - 1)) / growBy);
   }
   
   rudense_destroy(&dense);
}

int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_rudense_init),
      cmocka_unit_test(test_rudense_init_bitfield),
      cmocka_unit_test(test_rudense_add),
      cmocka_unit_test(test_rudense_remove),
      cmocka_unit_test(test_rudense_add_remove),
      cmocka_unit_test(test_rudense_get),
      cmocka_unit_test(test_rudense_getall),
      cmocka_unit_test(test_rudense_isaddress),
      cmocka_unit_test(test_rudense_iterator),
      cmocka_unit_test(test_rudense_mergeunused),
      cmocka_unit_test(test_rudense_grow),
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


