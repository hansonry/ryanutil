#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <math.h>


#define check_expected_double(parameter) \
    _check_expected(__func__, #parameter, __FILE__, __LINE__, \
                    cast_to_largest_integral_type(util_convert_from_double(parameter)))
                    
#define mock_double() util_convert_to_double(_mock(__func__, __FILE__, __LINE__))

union dtolit
{
   double d;
   LargestIntegralType lit;
};

static double util_convert_to_double(LargestIntegralType value)
{
   union dtolit u;
   u.d = 0;
   u.lit = value;
   return u.d;
}

static LargestIntegralType util_convert_from_double(double value)
{
   union dtolit u;
   u.lit = 0;
   u.d = value;
   return u.lit;
}


double frexp ( double arg, int * exp )
{
   function_called();
   check_expected_double(arg);
   check_expected_ptr(exp);
   if(exp != NULL)
   {
      *exp = (int)mock();
   }
   return mock_double();
}