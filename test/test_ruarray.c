#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include "ruarray.h"


struct thing
{
   int x;
   int y;
   int z;
};
static void util_set(struct thing * t, int x, int y, int z)
{
   t->x = x;
   t->y = y;
   t->z = z;
}

static void util_write(struct thing * thingbase, size_t count)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      util_set(&thingbase[i], i, i * -11, i + 4);
   }
}

static void util_verify_order(struct thing * thingbase, size_t count, size_t * order)
{
   int i;
   // try to write to everything
   for(i = 0; i < count; i++)
   {
      //printf("verify %i = %i\n", (int)i, (int)order[i]);
      assert_int_equal(order[i],       thingbase[i].x);
      assert_int_equal(order[i] * -11, thingbase[i].y);
      assert_int_equal(order[i] + 4,   thingbase[i].z);
   }
}

static void util_set_order(size_t * order, size_t arraySize, size_t toIndex, size_t fromIndex, size_t count)
{
   int i;
   // Init the order
   for(i = 0; i < arraySize; i++)
   {
      order[i] = i;
   }
   
   for(i = 0; i < count; i++)
   {
      if(toIndex + i < arraySize && 
         fromIndex + i < arraySize)
      {
         order[toIndex + i] = fromIndex + i;
      }
   }
   //for(i = 0; i < arraySize; i++) printf("o %i = %i\n", (int)i, (int)order[i]);
}   

static void test_ruarray_swap(void **state)
{
   struct thing list[10];
   struct thing swapSpace;

   util_set(&list[0], 1, 2, 3);
   util_set(&list[1], 4, 5, 6);

   util_set(&list[2], 7,  8,  9);
   util_set(&list[3], 10, 11, 12);

   ruarray_swap(list, sizeof(struct thing), &swapSpace, 0, 1); 

   assert_int_equal(1, list[1].x);
   assert_int_equal(2, list[1].y);
   assert_int_equal(3, list[1].z);

   assert_int_equal(4, list[0].x);
   assert_int_equal(5, list[0].y);
   assert_int_equal(6, list[0].z);

   ruarray_swap(list, sizeof(struct thing), &swapSpace, 2, 3); 

   assert_int_equal(7, list[3].x);
   assert_int_equal(8, list[3].y);
   assert_int_equal(9, list[3].z);

   assert_int_equal(10, list[2].x);
   assert_int_equal(11, list[2].y);
   assert_int_equal(12, list[2].z);
   
   // Test NULL Swap Space
   ruarray_swap(list, sizeof(struct thing), NULL, 2, 3); 

   assert_int_equal(7, list[2].x);
   assert_int_equal(8, list[2].y);
   assert_int_equal(9, list[2].z);

   assert_int_equal(10, list[3].x);
   assert_int_equal(11, list[3].y);
   assert_int_equal(12, list[3].z);
}

static void test_ruarray_copy_zero1(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 0, 0);
   
   ruarray_copy(list, sizeof(struct thing), 10, 0, 0, 0);
   
   util_verify_order(list, 10, order);
   

}

static void test_ruarray_copy_1t0s1(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 1, 1);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 0, 1, 1), 1);
   
   util_verify_order(list, 10, order);


}

static void test_ruarray_copy_1t0s2(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 1, 2);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 0, 1, 2), 2);
   
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_1t0s9(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 1, 9);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 0, 1, 9), 9);
   
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_0t2s6(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 2, 0, 6);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 2, 0, 6), 6);
 
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_0t10s1and2(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 10, 0, 1);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 10, 0, 1), 0);
 
   util_verify_order(list, 10, order);
   

   util_write(list, 10);
   util_set_order(order, 10, 10, 0, 2);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 10, 0, 2), 0);
 
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_0t9s1(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 9, 0, 1);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 9, 0, 1), 1);
 
   util_verify_order(list, 10, order);
}


static void test_ruarray_copy_0t9s2and3(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 9, 0, 2);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 9, 0, 2), 1);
 
   util_verify_order(list, 10, order);


   util_write(list, 10);
   util_set_order(order, 10, 9, 0, 3);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 9, 0, 3), 1);
 
   util_verify_order(list, 10, order);

}


static void test_ruarray_copy_0t1s20(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 1, 0, 20);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 1, 0, 20), 0);
 
   util_verify_order(list, 10, order);
}


static void test_ruarray_copy_0t10s1(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 10, 0, 1);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 10, 0, 1), 0);
 
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_10t0s1(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 10, 1);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 0, 10, 1), 0);
 
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_7t7s5(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 7, 7, 5);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 7, 7, 5), 3);
 
   util_verify_order(list, 10, order);
}

static void test_ruarray_copy_8t0s4(void **state)
{
   struct thing list[10];
   size_t order[10];

   util_write(list, 10);
   util_set_order(order, 10, 0, 8, 4);
   
   assert_int_equal(ruarray_copy(list, sizeof(struct thing), 10, 0, 8, 4), 2);
 
   util_verify_order(list, 10, order);
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_ruarray_swap),
        cmocka_unit_test(test_ruarray_copy_zero1),
        cmocka_unit_test(test_ruarray_copy_1t0s1),
        cmocka_unit_test(test_ruarray_copy_1t0s2),
        cmocka_unit_test(test_ruarray_copy_1t0s9),
        cmocka_unit_test(test_ruarray_copy_0t2s6),
        cmocka_unit_test(test_ruarray_copy_0t10s1and2),
        cmocka_unit_test(test_ruarray_copy_0t9s1),
        cmocka_unit_test(test_ruarray_copy_0t9s2and3),
        cmocka_unit_test(test_ruarray_copy_0t10s1),
        cmocka_unit_test(test_ruarray_copy_10t0s1),
        cmocka_unit_test(test_ruarray_copy_7t7s5),
        cmocka_unit_test(test_ruarray_copy_8t0s4),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}


