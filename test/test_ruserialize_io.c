#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <math.h>
#include "ruserialize.h"

static void test_ruserialize_i8(void **state)
{
   unsigned char buffer[255];
   unsigned int i;

   // Min and Max
   ruserialize_i8write(&buffer[1], 0);
   assert_int_equal(0, ruserialize_i8read(&buffer[1]));

   ruserialize_i8write(&buffer[1], 0xFF);
   assert_int_equal(0xFF, ruserialize_i8read(&buffer[1]));
   
   // Test Some Points
   for(i = 0; i < 0xFF; i += 9)
   {
      ruserialize_i8write(&buffer[1], i);
      assert_int_equal(i, ruserialize_i8read(&buffer[1]));      
   }
}

static void test_ruserialize_i16(void **state)
{
   unsigned char buffer[255];
   unsigned int i;

   // Min and Max
   ruserialize_i16write(&buffer[1], 0);
   assert_int_equal(0, ruserialize_i16read(&buffer[1]));

   ruserialize_i16write(&buffer[1], 0xFFFF);
   assert_int_equal(0xFFFF, ruserialize_i16read(&buffer[1]));
   
   // Test Some Points
   for(i = 0; i < 0xFFFF; i += 99)
   {
      ruserialize_i16write(&buffer[1], i);
      assert_int_equal(i, ruserialize_i16read(&buffer[1]));      
   }
}

static void test_ruserialize_i32(void **state)
{
   unsigned char buffer[255];
   unsigned int i;

   // Min and Max
   ruserialize_i32write(&buffer[1], 0);
   assert_int_equal(0, ruserialize_i32read(&buffer[1]));

   ruserialize_i32write(&buffer[1], 0xFFFFFFFF);
   assert_int_equal(0xFFFFFFFF, ruserialize_i32read(&buffer[1]));
   
   // Test Some Points
   for(i = 0; i < 0xFFFF; i += 9999)
   {
      ruserialize_i32write(&buffer[1], i);
      assert_int_equal(i, ruserialize_i32read(&buffer[1]));      
   }
}

static float util_compute_epsolon(float value, float e2)
{
   int exp;
   (void)frexp(value, &exp);
   return ldexp(e2, exp);
}

static void test_ruserialize_f16(void **state)
{
   unsigned char buffer[255];
   float i;
   

   // Min and Max
   ruserialize_f16write(&buffer[1], -65504.0f);
   assert_float_equal(-65504.0f, ruserialize_f16read(&buffer[1]), 0.01);

   ruserialize_f16write(&buffer[1], 0.0f);
   assert_float_equal(0.0f, ruserialize_f16read(&buffer[1]), 0.01);

   ruserialize_f16write(&buffer[1], 65504.0f);
   assert_float_equal(65504.0f, ruserialize_f16read(&buffer[1]), 0.01);
   
   // Test Some Points
   for(i = -65504.0f; i < 65504.0f; i += 99.99f)
   {
      ruserialize_f16write(&buffer[1], i);
      assert_float_equal(i, ruserialize_f16read(&buffer[1]), util_compute_epsolon(i, 0.001f)); 

   }
   
   // Test Some Points in scale
   for(i = 0.000000059605f; i < 65504.0f; i *=1.9f)
   {
      ruserialize_f16write(&buffer[1], i);
      assert_float_equal(i, ruserialize_f16read(&buffer[1]), util_compute_epsolon(i, 1.0f)); 
   }
   
}

static void test_ruserialize_f32(void **state)
{
   unsigned char buffer[255];
   double i;
   double value;
   const double max_value = ldexp(1, 127) * (2 - ldexp(1, -23));
   // There are limmits to what we can do with cmocka.
   const double min_nonzero_value = ldexp(1, -120); 
   

   // Min and Max
   value = -max_value;
   ruserialize_f32write(&buffer[1], value);
   assert_float_equal(value, ruserialize_f32read(&buffer[1]), 0.01);

   value = 0.0;
   ruserialize_f32write(&buffer[1], value);
   assert_float_equal(value, ruserialize_f32read(&buffer[1]), 0.01);

   value = max_value;
   ruserialize_f32write(&buffer[1], value);
   assert_float_equal(value, ruserialize_f32read(&buffer[1]), 0.01);
   
   // Test Some Points
   for(i = -max_value; i < max_value; i += max_value / 99.9)
   {
      ruserialize_f32write(&buffer[1], i);
      assert_float_equal(i, ruserialize_f32read(&buffer[1]), util_compute_epsolon(i, 0.001f)); 

   }
   
   // Test Some Points in scale
   for(i = min_nonzero_value; i < max_value; i *=1.9f)
   {
      ruserialize_f32write(&buffer[1], i);
      assert_float_equal(i, ruserialize_f32read(&buffer[1]), util_compute_epsolon(i, 1.0f)); 
   }
   
}



int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_ruserialize_i8),
      cmocka_unit_test(test_ruserialize_i16),
      cmocka_unit_test(test_ruserialize_i32),
      cmocka_unit_test(test_ruserialize_f16),
      cmocka_unit_test(test_ruserialize_f32),
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


