#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <math.h>
#include "ruserialize.h"

static void test_ruserialize_i8write(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   ruserialize_i8write(&buffer[1], 5);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 5);
   assert_int_equal(buffer[2], 0);

   ruserialize_i8write(&buffer[1], 7);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 7);
   assert_int_equal(buffer[2], 0);

   ruserialize_i8write(&buffer[1], 0xFF);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0);

   ruserialize_i8write(&buffer[1], 0);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);

   ruserialize_i8write(&buffer[1], (uint8_t)0xFFFF);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0);
 
   ruserialize_i8write(&buffer[2], 7);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 7);
   assert_int_equal(buffer[3], 0);

}

static void test_ruserialize_i8read(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   buffer[1] = 5;
   assert_int_equal(5, ruserialize_i8read(&buffer[1]));
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 5);
   assert_int_equal(buffer[2], 0);

   buffer[1] = 7;
   assert_int_equal(7, ruserialize_i8read(&buffer[1]));

   buffer[1] = 0xFF;
   assert_int_equal(0xFF, ruserialize_i8read(&buffer[1]));

   buffer[0] = 0xFF;
   buffer[1] = 0;
   buffer[2] = 0xFF;
   assert_int_equal(0, ruserialize_i8read(&buffer[1]));


   buffer[1] = 0;
   buffer[2] = 7;
   buffer[3] = 0xFF;
   assert_int_equal(7, ruserialize_i8read(&buffer[2]));
}

static void test_ruserialize_i16write(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   ruserialize_i16write(&buffer[1], 5);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 5);
   assert_int_equal(buffer[3], 0);


   ruserialize_i16write(&buffer[1], 0x2356);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x23);
   assert_int_equal(buffer[2], 0x56);
   assert_int_equal(buffer[3], 0);
   
   ruserialize_i16write(&buffer[1], (uint16_t)0x123456);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x34);
   assert_int_equal(buffer[2], 0x56);
   assert_int_equal(buffer[3], 0);

   ruserialize_i16write(&buffer[3], 0x23);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x34);
   assert_int_equal(buffer[2], 0x56);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0x23);
   assert_int_equal(buffer[5], 0);

   ruserialize_i16write(&buffer[1], 0);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);
   assert_int_equal(buffer[3], 0);

   ruserialize_i16write(&buffer[1], 0xFFFF);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0);

}

static void test_ruserialize_i16read(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   buffer[1] = 0;
   buffer[2] = 5;
   assert_int_equal(5, ruserialize_i16read(&buffer[1]));
   assert_int_equal(0, ruserialize_i16read(&buffer[0]));
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 5);
   assert_int_equal(buffer[3], 0);

   buffer[1] = 0x12;
   buffer[2] = 0x34;
   assert_int_equal(0x1234, ruserialize_i16read(&buffer[1]));
   assert_int_equal(0x12, ruserialize_i16read(&buffer[0]));
   assert_int_equal(0x3400, ruserialize_i16read(&buffer[2]));

   buffer[1] = 0xFF;
   buffer[2] = 0xFF;
   assert_int_equal(0xFFFF, ruserialize_i16read(&buffer[1]));
   
}

static void test_ruserialize_i32write(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   ruserialize_i32write(&buffer[1], 5);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 5);
   assert_int_equal(buffer[5], 0);


   ruserialize_i32write(&buffer[1], 0x12345678);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x12);
   assert_int_equal(buffer[2], 0x34);
   assert_int_equal(buffer[3], 0x56);
   assert_int_equal(buffer[4], 0x78);
   assert_int_equal(buffer[5], 0);

   ruserialize_i32write(&buffer[6], 0x87654321);
   assert_int_equal(buffer[5], 0);
   assert_int_equal(buffer[6], 0x87);
   assert_int_equal(buffer[7], 0x65);
   assert_int_equal(buffer[8], 0x43);
   assert_int_equal(buffer[9], 0x21);
   assert_int_equal(buffer[10], 0);

   ruserialize_i32write(&buffer[1], 0xFFFFFFFF);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0xFF);
   assert_int_equal(buffer[4], 0xFF);
   assert_int_equal(buffer[5], 0);

   ruserialize_i32write(&buffer[1], 0x1234);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);
   assert_int_equal(buffer[3], 0x12);
   assert_int_equal(buffer[4], 0x34);
   assert_int_equal(buffer[5], 0);

   ruserialize_i32write(&buffer[1], 0);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0);
   assert_int_equal(buffer[5], 0);
}

static void test_ruserialize_i32read(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   
   buffer[1] = 0;
   buffer[2] = 0;
   buffer[3] = 0;
   buffer[4] = 5;
   assert_int_equal(5, ruserialize_i32read(&buffer[1]));
   assert_int_equal(0, ruserialize_i32read(&buffer[0]));
   assert_int_equal(0x500, ruserialize_i32read(&buffer[2]));
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0);
   assert_int_equal(buffer[2], 0);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 5);
   assert_int_equal(buffer[5], 0);



   buffer[6] = 0x12;
   buffer[7] = 0x34;
   buffer[8] = 0x56;
   buffer[9] = 0x78;
   assert_int_equal(0x12345678, ruserialize_i32read(&buffer[6]));


   buffer[6] = 0x87;
   buffer[7] = 0x65;
   buffer[8] = 0x43;
   buffer[9] = 0x21;
   assert_int_equal(0x87654321, ruserialize_i32read(&buffer[6]));


   buffer[11] = 0xFF;
   buffer[12] = 0xFF;
   buffer[13] = 0xFF;
   buffer[14] = 0xFF;
   assert_int_equal(0xFFFFFFFF, ruserialize_i32read(&buffer[11]));


   buffer[11] = 0x00;
   buffer[12] = 0x00;
   buffer[13] = 0x12;
   buffer[14] = 0x34;
   assert_int_equal(0x1234, ruserialize_i32read(&buffer[11]));
   
}

#define expect_double_value(function, parameter, value) \
    expect_value_count(function, parameter, util_convert_from_double(value), 1)
    
#define will_return_double(function, value) \
    _will_return(#function, __FILE__, __LINE__, \
                 cast_to_largest_integral_type(util_convert_from_double(value)), 1)

union dtolit
{
   double d;
   LargestIntegralType lit;
};

static LargestIntegralType util_convert_from_double(double value)
{
   union dtolit u;
   u.lit = 0;
   u.d = value;
   return u.lit;
}

static void test_ruserialize_f16write(void **state)
{
   unsigned char buffer[255];
   float value;
   memset(buffer, 0, sizeof(buffer));

   // Testing Positive Normal Numbers (Min and Max)
   value = 0.00006103515625f;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -13 );
   will_return_double(   frexp, 0.5 );
   
   
   ruserialize_f16write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x04);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);

   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 65504.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 16 );
   will_return_double(   frexp, 0.99951171875 );


   ruserialize_f16write(&buffer[1], 65504.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x7B);
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0);

   // Testing Negative Normal Numbers (Min and Max)
   value = -0.00006103515625f;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg,value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -13 );
   will_return_double(   frexp, -0.5 ); 
   
   
   ruserialize_f16write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x84);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);

   expect_function_call( frexp );
   expect_double_value(  frexp, arg, -65504.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 16 );
   will_return_double(   frexp, -0.99951171875 );


   ruserialize_f16write(&buffer[1], -65504.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFB);
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0);


   // Testing Positive Subnormal Numbers (Min and Max)
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 0.000000059605f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -23 );
   will_return_double(   frexp, 0.5 );
   
   
   ruserialize_f16write(&buffer[1], 0.000000059605f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x01);
   assert_int_equal(buffer[3], 0);
   
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 0.000060976f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -14 );
   will_return_double(   frexp, 0.9990234375); // Computed: 0.999031


   ruserialize_f16write(&buffer[1], 0.000060976f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x03);
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0);
   
   // Testing 0
   ruserialize_f16write(&buffer[1], 0.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);
   
   // Testing -0
   ruserialize_f16write(&buffer[1], -0.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x80);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);
   
   // Testing 1 and -1
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 1.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 1 );
   will_return_double(   frexp, 0.5 ); 


   ruserialize_f16write(&buffer[1], 1.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x3C);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);
   

   expect_function_call( frexp );
   expect_double_value(  frexp, arg, -1.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 1 );
   will_return_double(   frexp, -0.5 ); 


   ruserialize_f16write(&buffer[1], -1.0f);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xBC);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);
   
   // Testing infinity and -infinity
   ruserialize_f16write(&buffer[1], INFINITY);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x7C);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);

   ruserialize_f16write(&buffer[1], -INFINITY);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFC);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0);
   
   // Testing NAN
   ruserialize_f16write(&buffer[1], NAN);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1] & 0x7F, 0x7F); // I don't care about the sign here (I don't think)
   assert_int_equal(buffer[2], 0xFF);
   assert_int_equal(buffer[3], 0);
   
   // Testing a different address pointer
   value = 0.00006103515625f;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -13 );
   will_return_double(   frexp, 0.5 );
   
   
   ruserialize_f16write(&buffer[4], value);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0x04);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);
   
   // Testing Small Value Rounds to Zero
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 0.000000019605f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -25 );
   will_return_double(   frexp, 0.657835 );
   
   ruserialize_f16write(&buffer[4], 0.000000019605f);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);
   
   // Testing Large Values round to inf
   value = 65536.0f;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 17 );
   will_return_double(   frexp, 0.5 );
   
   ruserialize_f16write(&buffer[4], value);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0x7C);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);


   expect_function_call( frexp );
   expect_double_value(  frexp, arg, 105520.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 17 );
   will_return_double(   frexp, 0.805054 );
   
   ruserialize_f16write(&buffer[4], 105520.0f);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0x7C);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);

   value = -65536.0f;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 17 );
   will_return_double(   frexp, -0.5 );
   
   ruserialize_f16write(&buffer[4], value);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0xFC);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);


   expect_function_call( frexp );
   expect_double_value(  frexp, arg, -105520.0f);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 17 );
   will_return_double(   frexp, -0.805054 );
   
   ruserialize_f16write(&buffer[4], -105520.0f);
   assert_int_equal(buffer[3], 0);
   assert_int_equal(buffer[4], 0xFC);
   assert_int_equal(buffer[5], 0x00);
   assert_int_equal(buffer[6], 0);
   

}

static void test_ruserialize_f16read(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));

   // Testing Positive Normal Numbers (Min and Max)
   
   buffer[1] = 0x04;
   buffer[2] = 0x00;
   assert_float_equal(0.000061035f, ruserialize_f16read(&buffer[1]), 0.000000001);

   buffer[1] = 0x7B;
   buffer[2] = 0xFF;
   assert_float_equal(65504.0f, ruserialize_f16read(&buffer[1]), 0.000000001);

   // Testing Negative Normal Numbers (Min and Max)

   buffer[1] = 0x84;
   buffer[2] = 0x00;
   assert_float_equal(-0.000061035f, ruserialize_f16read(&buffer[1]), 0.000000001);

   buffer[1] = 0xFB;
   buffer[2] = 0xFF;
   assert_float_equal(-65504.0f, ruserialize_f16read(&buffer[1]), 0.000000001);
   
   // Testing Positive Subnormal Numbers (Min and Max)

   buffer[1] = 0x00;
   buffer[2] = 0x01;
   assert_float_equal(0.000000059605f, ruserialize_f16read(&buffer[1]), 0.000000001);

   buffer[1] = 0x03;
   buffer[2] = 0xFF;
   assert_float_equal(0.000060976f, ruserialize_f16read(&buffer[1]), 0.000000001);

   // Testing 0

   buffer[1] = 0x00;
   buffer[2] = 0x00;
   assert_float_equal(0.0f, ruserialize_f16read(&buffer[1]), 0.000000001);
   assert_false(signbit(ruserialize_f16read(&buffer[1])));

   // Testing -0
   buffer[1] = 0x80;
   buffer[2] = 0x00;
   assert_float_equal(-0.0f, ruserialize_f16read(&buffer[1]), 0.000000001);
   assert_true(signbit(ruserialize_f16read(&buffer[1])));
   
   
   // Testing 1 and -1
   buffer[1] = 0x3C;
   buffer[2] = 0x00;
   assert_float_equal(1.0f, ruserialize_f16read(&buffer[1]), 0.000000001);
   
   buffer[1] = 0xBC;
   buffer[2] = 0x00;
   assert_float_equal(-1.0f, ruserialize_f16read(&buffer[1]), 0.000000001);

   // Testing infinity and -infinity

   buffer[1] = 0x7C;
   buffer[2] = 0x00;
   assert_float_equal(INFINITY, ruserialize_f16read(&buffer[1]), 0.000000001);

   buffer[1] = 0xFC;
   buffer[2] = 0x00;
   assert_float_equal(-INFINITY, ruserialize_f16read(&buffer[1]), 0.000000001);

   // Testing NAN
   buffer[1] = 0x7F;
   buffer[2] = 0xFF;
   assert_true(isnan(ruserialize_f16read(&buffer[1])));
   
   // Testing a different address pointer
   buffer[4] = 0x04;
   buffer[5] = 0x00;
   assert_float_equal(0.000061035f, ruserialize_f16read(&buffer[4]), 0.000000001);
}


static void test_ruserialize_f32write(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));
   double value;

   // Testing Positive Normal Numbers (Min and Max)
   
   value = ldexp(1, -126);
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -125 );
   will_return_double(   frexp, 0.5 );
   
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   value = ldexp(1, 127) * (2 - ldexp(1, -23));
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 128 );
   will_return_double(   frexp, 0.9999999403954 );
   
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x7F);
   assert_int_equal(buffer[2], 0x7F);
   assert_int_equal(buffer[3], 0xFF);
   assert_int_equal(buffer[4], 0xFF);
   assert_int_equal(buffer[5], 0);



   // Testing Negative Normal Numbers (Min and Max)
   value = -ldexp(1, -126);
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -125 );
   will_return_double(   frexp, 0.5 );
   
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x80);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   value = -ldexp(1, 127) * (2 - ldexp(1, -23));
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 128 );
   will_return_double(   frexp, 0.9999999403954 );
   
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0x7F);
   assert_int_equal(buffer[3], 0xFF);
   assert_int_equal(buffer[4], 0xFF);
   assert_int_equal(buffer[5], 0);

   // Testing Positive Subnormal Numbers (Min and Max)

   value = ldexp(1, -126) * ldexp(1, -23);
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -148 );
   will_return_double(   frexp, 0.5 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x01);
   assert_int_equal(buffer[5], 0);


   value = ldexp(1, -126) * (1 - ldexp(1, -23));
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -126 );
   will_return_double(   frexp, 0.99999988079071044921875 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x7F);
   assert_int_equal(buffer[3], 0xFF);
   assert_int_equal(buffer[4], 0xFF);
   assert_int_equal(buffer[5], 0);

   // Testing 0
   value = 0.0;
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);
   
   // Testing -0
   value = -0.0;
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x80);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   // Testing 1 and -1
   value = 1.0;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 1 );
   will_return_double(   frexp, 0.5 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x3F);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   value = -1.0;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 1 );
   will_return_double(   frexp, -0.5 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xBF);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   // Testing infinity and -infinity
   ruserialize_f32write(&buffer[1], INFINITY);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x7F);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   ruserialize_f32write(&buffer[1], -INFINITY);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);
   
   // Testing NAN
   ruserialize_f32write(&buffer[1], NAN);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF); // High bit is not specified for NAN.
   assert_int_equal(buffer[2], 0x80); // Using Signal NaN because it felt good.
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x01); 
   assert_int_equal(buffer[5], 0);
   
   
   // Testing a different address pointer
   value = 1.0;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 1 );
   will_return_double(   frexp, 0.5 );
   
   ruserialize_f32write(&buffer[6], value);
   assert_int_equal(buffer[5], 0);
   assert_int_equal(buffer[6], 0x3F);
   assert_int_equal(buffer[7], 0x80);
   assert_int_equal(buffer[8], 0x00);
   assert_int_equal(buffer[9], 0x00);
   assert_int_equal(buffer[10], 0);

   // Testing Small Value Rounds to Zero
   value = ldexp(1, -126) * ldexp(1, -24);
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, -149 );
   will_return_double(   frexp, 0.5 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x00);
   assert_int_equal(buffer[2], 0x00);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   // Testing Large Values round to inf
   value = ldexp(1, 128) * (2 - ldexp(1, -23));
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 129 );
   will_return_double(   frexp, 0.999999940395355224609375 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x7F);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);

   value = -ldexp(1, 128) * (2 - ldexp(1, -23));
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 129 );
   will_return_double(   frexp, -0.999999940395355224609375 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0xFF);
   assert_int_equal(buffer[2], 0x80);
   assert_int_equal(buffer[3], 0x00);
   assert_int_equal(buffer[4], 0x00);
   assert_int_equal(buffer[5], 0);
   
   // Testing PI
   value = 3.14159274101257324219;
   expect_function_call( frexp );
   expect_double_value(  frexp, arg, value);
   expect_not_value(     frexp, exp, NULL);
   will_return(          frexp, 2 );
   will_return_double(   frexp, 0.785398185253143310546875 );
   
   ruserialize_f32write(&buffer[1], value);
   assert_int_equal(buffer[0], 0);
   assert_int_equal(buffer[1], 0x40);
   assert_int_equal(buffer[2], 0x49);
   assert_int_equal(buffer[3], 0x0F);
   assert_int_equal(buffer[4], 0xDB);
   assert_int_equal(buffer[5], 0);

}

static void test_ruserialize_f32read(void **state)
{
   unsigned char buffer[255];
   memset(buffer, 0, sizeof(buffer));

   // Testing Positive Normal Numbers (Min and Max)
   buffer[1] = 0x04;
   buffer[2] = 0x00;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(ldexp(1, -126), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

   buffer[1] = 0x7F;
   buffer[2] = 0x7F;
   buffer[3] = 0xFF;
   buffer[4] = 0xFF;
   assert_float_equal(ldexp(1, 127) * (2 - ldexp(1, -23)), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

   // Testing Negative Normal Numbers (Min and Max)
   buffer[1] = 0x84;
   buffer[2] = 0x00;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(-ldexp(1, -126), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

   buffer[1] = 0xFF;
   buffer[2] = 0x7F;
   buffer[3] = 0xFF;
   buffer[4] = 0xFF;
   assert_float_equal(-ldexp(1, 127) * (2 - ldexp(1, -23)), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

   // Testing Positive Subnormal Numbers (Min and Max)
   buffer[1] = 0x00;
   buffer[2] = 0x00;
   buffer[3] = 0x00;
   buffer[4] = 0x01;
   assert_float_equal(ldexp(1, -149), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

   buffer[1] = 0x00;
   buffer[2] = 0x7F;
   buffer[3] = 0xFF;
   buffer[4] = 0xFF;
   assert_float_equal(ldexp(1, -126) * (1 - ldexp(1, -23)), 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
                      
   // Testing 0
   buffer[1] = 0x00;
   buffer[2] = 0x00;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(0, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
   assert_false(signbit(ruserialize_f32read(&buffer[1])));
   
   // Testing -0
   buffer[1] = 0x80;
   buffer[2] = 0x00;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(-0, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
   assert_true(signbit(ruserialize_f32read(&buffer[1])));

   // Testing 1 and -1
   buffer[1] = 0x3F;
   buffer[2] = 0x80;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(1, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
                      
   buffer[1] = 0xBF;
   buffer[2] = 0x80;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(-1, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
                      
   // Testing infinity and -infinity
   buffer[1] = 0x7F;
   buffer[2] = 0x80;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(INFINITY, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
   assert_true(isinf(ruserialize_f32read(&buffer[1])));

   buffer[1] = 0xFF;
   buffer[2] = 0x80;
   buffer[3] = 0x00;
   buffer[4] = 0x00;
   assert_float_equal(-INFINITY, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);
   assert_true(isinf(ruserialize_f32read(&buffer[1])));
   
   // Testing NAN
   buffer[1] = 0xFF;
   buffer[2] = 0x80;
   buffer[3] = 0x00;
   buffer[4] = 0x01;
   assert_true(isnan(ruserialize_f32read(&buffer[1])));

   // Testing a different address pointer
   buffer[6] = 0x3F;
   buffer[7] = 0x80;
   buffer[8] = 0x00;
   buffer[9] = 0x00;
   assert_float_equal(1, 
                      ruserialize_f32read(&buffer[6]), 
                      0.000000001);
                      
   // Testing PI
   buffer[1] = 0x40;
   buffer[2] = 0x49;
   buffer[3] = 0x0F;
   buffer[4] = 0xDB;
   assert_float_equal(3.14159274101257324219, 
                      ruserialize_f32read(&buffer[1]), 
                      0.000000001);

}



int main(void) {
   const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_ruserialize_i8write),
      cmocka_unit_test(test_ruserialize_i8read),
      cmocka_unit_test(test_ruserialize_i16write),
      cmocka_unit_test(test_ruserialize_i16read),
      cmocka_unit_test(test_ruserialize_i32write),
      cmocka_unit_test(test_ruserialize_i32read),
      cmocka_unit_test(test_ruserialize_f16write),
      cmocka_unit_test(test_ruserialize_f16read),
      cmocka_unit_test(test_ruserialize_f32write),
      cmocka_unit_test(test_ruserialize_f32read),
   };

   return cmocka_run_group_tests(tests, NULL, NULL);
}


