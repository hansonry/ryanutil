#ifndef __RUMEM_H__
#define __RUMEM_H__
#include <stddef.h>
#include "rustatus.h"

struct rumem
{
   int baseByteOffset;
   size_t size;
   size_t elementSize;
   void * swapSpace;
};

#define rumem_initm(mem, base, initSize)  rumem_init((mem), (void**)&(base), sizeof(*(base)), (initSize))

void rumem_init(struct rumem * mem, void ** basePtr, size_t elementSize, size_t initSize);

void rumem_destroy(struct rumem * mem);

void rumem_growto(struct rumem * mem, size_t targetSize);
void rumem_shrinkto(struct rumem * mem, size_t targetSize);

void * rumem_get(struct rumem * mem, size_t index);

enum rustatus rumem_copyin(struct rumem * mem, size_t index, const void * data);
void rumem_copyinall(struct rumem * mem, const void * data);
size_t rumem_copyinto(struct rumem * mem, const void * data, size_t index, size_t size);
enum rustatus rumem_copyout(struct rumem * mem, void * data, size_t index);

size_t rumem_indexof(struct rumem * mem, const void * address);

enum rustatus rumem_swap(struct rumem * mem, size_t index1, size_t index2);

size_t rumem_blockcopy(struct rumem * mem, size_t toIndex, 
                       size_t fromIndex, size_t count);
                       
size_t rumem_size(struct rumem * mem);

#endif // __RUMEM_H__

