#ifndef __RUDENSE_H__
#define __RUDENSE_H__
#include <stddef.h>
#include <stdbool.h>
#include "rustatus.h"

struct rudense
{
   unsigned char * base;
   size_t size;
   size_t count;
   size_t growBy;
   size_t * componentTable;
   size_t componentTableSize;
   size_t bitFieldSize;
};

void rudense_init(struct rudense * dense, size_t * componentTable, 
                                          size_t componentTableSize, 
                                          size_t initSize,
                                          size_t growBy);

void rudense_destroy(struct rudense * dense);

size_t rudense_add(struct rudense * dense, size_t * components, 
                                           size_t componentsSize);

void * rudense_get(struct rudense * dense, size_t address, size_t component);
void * rudense_getall(struct rudense * dense, size_t address, void * data);

enum rustatus rudense_remove(struct rudense * dense, size_t address);

bool rudense_isaddress(struct rudense * dense, size_t address);

void rudense_mergeunused(struct rudense * dense);

size_t rudense_iterstart(struct rudense * dense);
bool rudense_iternext(struct rudense * dense, size_t * address);

#endif // __RUDENSE_H__

