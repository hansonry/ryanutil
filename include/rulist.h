#ifndef __RULIST_H__
#define __RULIST_H__
#include <stddef.h>
#include <rumem.h>
#include <rustatus.h>

struct rulist
{
   struct rumem mem;
   size_t count;
   size_t growBy;
};

#define rulist_initm(list, base, growBy)  rulist_init((list), (void**)&(base), sizeof(*(base)), (growBy))

void rulist_init(struct rulist * list, void ** basePtr, size_t elementSize, size_t growBy);

void rulist_destroy(struct rulist * list);

enum rustatus rulist_swap(struct rulist * list, size_t index1, size_t index2);

size_t rulist_blockcopy(struct rulist * list, size_t toIndex, size_t fromIndex, size_t count);


void * rulist_add(struct rulist * list, size_t * newIndex);
void * rulist_addcopy(struct rulist * list, void * source, size_t * newIndex);

enum rustatus rulist_removefast(struct rulist * list, size_t index);
enum rustatus rulist_removeordered(struct rulist * list, size_t index);

void rulist_clear(struct rulist * list);

void * rulist_insert(struct rulist * list, size_t index);

enum rustatus rulist_copyin(struct rulist * list, size_t index, const void * data);
enum rustatus rulist_copyout(struct rulist * list, void * data, size_t index);
size_t rulist_copyinto(struct rulist * list, const void * data, size_t index, size_t size);
void rulist_copyinall(struct rulist * list, const void * data);

size_t rulist_indexof(struct rulist * list, const void * address);

size_t rulist_count(struct rulist * list);


#endif // __RULIST_H__
