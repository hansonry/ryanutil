#ifndef __RUARRAY_H__
#define __RUARRAY_H__
#include <stddef.h>

void ruarray_swap(void * array, size_t elementSize, void * swapSpace, 
                  size_t index1, size_t index2);

size_t ruarray_copy(void * array, size_t elementSize, size_t arraySize,
                    size_t toIndex, size_t fromIndex, size_t count);

#endif // __RUARRAY_H__

