#ifndef __RUSERIALIZE_H__
#define __RUSERIALIZE_H__
#include <stdint.h>

void     ruserialize_i8write(void * buffer, uint8_t value);
uint8_t  ruserialize_i8read(const void * buffer);

void     ruserialize_i16write(void * buffer, uint16_t value);
uint16_t ruserialize_i16read(const void * buffer);

void     ruserialize_i32write(void * buffer, uint32_t value);
uint32_t ruserialize_i32read(const void * buffer);

void     ruserialize_i32write(void * buffer, uint32_t value);
uint32_t ruserialize_i32read(const void * buffer);

void     ruserialize_f16write(void * buffer, float value);
float    ruserialize_f16read(const void * buffer);

void     ruserialize_f32write(void * buffer, double value);
double   ruserialize_f32read(const void * buffer);

#endif // __RUSERIALIZE_H__
