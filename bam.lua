Import("test/testbuild.lua")


settings = NewSettings()

if family == "windows" then
   settings.cc.defines:Add("_CRT_SECURE_NO_WARNINGS")
   settings.cc.defines:Add("strdup=_strdup")   
end


settings.cc.includes:Add("include")

ryanutilSources = Collect("src/*.c")
ryantutilObjects = Compile(settings, ryanutilSources)
ryanutilLibrary = StaticLibrary(settings, "ryanutil", ryantutilObjects)



